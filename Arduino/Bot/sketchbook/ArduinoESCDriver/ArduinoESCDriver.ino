#include <WireB.h>
#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#define UDP_TX_PACKET_MAX_SIZE 32

// Give our Ethernet adapter an identity:
byte mac[] = { 0x90, 0xA2, 0xDA, 0x00, 0x04, 0xD7 };
IPAddress ip(192, 168, 1, 254);
unsigned int localPort = 8888;
// Handy-dandy packet buffer for UDP communications:
uint8_t packetBuffer[UDP_TX_PACKET_MAX_SIZE];
// Create an EthernetUDP instance to let us send and receive packets over UDP:
EthernetUDP Udp;

// Define i2c address of PCA9685:
#define I2C_ADDRESS 0x40
// Define PWM board CPU speed. Note this does not actually SET
// the PWM CPU speed, but sets our code's assumptions about
// what it is, since there's no way to actually query the chip
// for its CPU speed. Supposedly it's 25MHz by default, but
// in testing, 25.5MHz is much more accurate.
#define PWM_CPU_SPEED 26750000L
//#define PWM_CPU_SPEED 24500000L
// Define desired PWM rate in Hz:
#define PWM_RATE 100
// Define beginning pin number for H-Bridge Directional Controls
#define HBRIDGE_BEGINPIN 2
// How long to wait for input from network before disabling
// motor outputs. (Unit is "number of loop iterations")
#define EMERGENCY_STOP_TIME 12000

// Pin definition for emergency stop control:
#define EMERGENCY_STOP_PIN A3

// Define a variable to store the ticks in 1ms value, calculated
// during initialization of PCA9685:
float ticksIn1ms = 0;
float scaler = 0;
float hbridgeScaler = 0;
uint16_t offValue = 0;

// Emergency stop counter. Starts at zero, counts up every iteration of
// the main loop where the Arduino doesn't receive anything from the surface.
// Thus, after a certain time of inactivity, the Arduino will turn off the
// motors.
uint16_t emergencyStopCounter = 0;
uint8_t emergencyStopPacket[17] = {'M', 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128};
boolean emergencyStopped = false;

void setup() {  
  //Serial.begin(9600);
  
  // Emergency stop control pin.
  pinMode(EMERGENCY_STOP_PIN, OUTPUT);
  digitalWrite(EMERGENCY_STOP_PIN, LOW);
  
  // Hold pin 10 low to enable Ethernet shield.
  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);

  // Set pinmode for H-bridge output pins.
  for (int i = 0; i < 8; i++) {
    pinMode(HBRIDGE_BEGINPIN + i, OUTPUT);
    digitalWrite(HBRIDGE_BEGINPIN + i, LOW);
  }

  // start the Ethernet and UDP:
  Ethernet.begin(mac,ip);
  Udp.begin(localPort);

  // Start i2c bus:
  Wire.begin();
  initPWMBoard();
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if(packetSize)
  {
    // read the packet into packetBufffer.
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    
    // If packet starts with an ASCII "M" and packet is 17 bytes long,
    // send to PWM update routine.
    // If packet starts with ASCII "S" assume it's for sensors, see below.
    switch (packetBuffer[0]) {
      case 'M':
        if (packetSize == 17) {
          updatePWMOutputs(packetBuffer);
          Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
          Udp.write("OK");
          Udp.endPacket();
        } else {
          Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
          Udp.write("LEN");
          Udp.endPacket();
        }
      break;
      
      // Sensor I/O (i2c bus). Q = sensor query, W = sensor write.
      // Can read/write up to 16 bytes at a time to a sensor.
      // SQXY, X = i2c address, Y = num bytes to read
      // SWXVVVV, X = i2c address, VVVV = data to send (up to 16 bytes)
      case 'S':
        switch (packetBuffer[1]) {
          case 'Q':
            if (packetSize == 4 && packetBuffer[3] <= 16) {
              sensorQuery(packetBuffer);
            } else {
              Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
              Udp.write("REQ LEN");
              Udp.endPacket();
            }
          break;
          case 'W':
            if (packetSize <= 19) {
              sensorWrite(packetBuffer, packetSize);
            } else {
              Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
              Udp.write("WR LEN");
              Udp.endPacket();
            }
          break;
          default:
            Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
            Udp.write("SENS ERR");
            Udp.endPacket();
          break;
        }
      break;
      default:
          Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
          Udp.write("CMD ERR");
          Udp.endPacket();
      break;
    }
    // Reset the emergency stop counter, we got a packet!
    // Also, ff we had been previously emergency stopped,
    // re-enable the outputs now that we're in a known state.
    emergencyStopCounter = 0;
    if (emergencyStopped == true) {
      emergencyStopped = false;
      digitalWrite(EMERGENCY_STOP_PIN, LOW);
    }
  } else {
    if (emergencyStopped == false) {
      // Didn't receive a network packet...
      // Increment watchdog timer.
      emergencyStopCounter = emergencyStopCounter + 1;
      // If timer hits a threshold and no packet
      // has been received, power down all motors.
      if (emergencyStopCounter > EMERGENCY_STOP_TIME) {
        emergencyStopCounter = 0;
        digitalWrite(EMERGENCY_STOP_PIN, HIGH);
        emergencyStopped = true;
      }
    }
  }
  //delay(10);
}

// This function should initialize PCA9685.
void initPWMBoard() {
  // Reset board. i2c reset:
  Wire.beginTransmission(0x00);
  Wire.write(0x06);
  Wire.endTransmission();
  
  // Get/set prescale register value to something usable.
  // * Calculate desired prescale based on ticks in 1ms value
  //
  // Calculate # ticks in 1ms: (hey the 4096's cancel)
    // 1. Get PWM Clockrate:
    //    * CPU_Speed / (4096 * (prescaleRegister + 1))
    // 2. Find # of ticks in 1ms:
    //    * PWM Clockrate * 4.096 = # ticks in 1ms.
  // Register 0xFE is prescale register.
  uint8_t prescaleValue = round((float)PWM_CPU_SPEED / (4096.0 * (float)PWM_RATE)) - 1; // Straight from datasheet
  ticksIn1ms = (float)PWM_CPU_SPEED / (1000.0 * ((float)prescaleValue + 1.0));
  scaler = ticksIn1ms / 255.0;
  hbridgeScaler = 4095 / 128.0;
  
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0xFE);
  Wire.write(prescaleValue);
  Wire.endTransmission();
  
  //// Init board:
  //// MODE1 defaults to (0b00010001)
  //// ...set MODE1 to 0b00100001. This causes:
  // Set auto-increment on PWM board
  // * MODE1 (reg. 0x00) set bit 5.
  // Wake board up
  // * MODE1 (reg. 0x00) clear bit 4.
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x00);
  Wire.write(0x21);
  Wire.endTransmission();
}

// This function should take an array of 16 uint8_t's for throttle levels.
// For each value, use data about PWM board to calculate appropriate
// off-time and write it all to the board at once.
//
// For the first 8 outputs, just send PWM 1ms - 2ms timing controls.
// For outputs 9 through 16, send 0 to 100% PWM to the PWM board
// and toggle direction via an I/O pin (HBRIDGE_BEGINPIN and so on)
void updatePWMOutputs(uint8_t PWMChannel[]) {
  
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(0x06); // First channel is at register 0x06.
  
  // Channels 1 through 8.
  for (int i = 1; i < 9; i++) {
    Wire.write(0x00); // On time is always zero.
    Wire.write(0x00);
    uint16_t offValue = (uint16_t)(round(ticksIn1ms + PWMChannel[i] * scaler)); // This line might need tweaking for performance reasons.
    Wire.write(offValue & 0x00FF); // Off time LOW byte.
    Wire.write(offValue >> 8); // Off time HIGH byte
  }
  
  // Channels 9 through 16.
  // < 124: reverse power
  // 126-130 = center
  // > 132: forwards power
  for (int i = 9; i < 17; i++) {
    uint16_t onValue = 0;
    if (PWMChannel[i] == 255) {
      // Send full-on command, direction HIGH
      onValue = 4096;
      offValue = 0;
      digitalWrite(HBRIDGE_BEGINPIN + (i - 9), HIGH);
    } else if (PWMChannel[i] == 0) {
      // Send full-off command, direction LOW
      onValue = 4096;
      offValue = 0;
      digitalWrite(HBRIDGE_BEGINPIN + (i - 9), LOW);
    } else if (PWMChannel[i] > 132) {
      // PWM Channel scale from 0 to 100%
      // PWM Direction HIGH
      onValue = 0;
      offValue = (uint16_t)((PWMChannel[i] - 128) * hbridgeScaler); // value from 0 to 4095
      digitalWrite(HBRIDGE_BEGINPIN + (i - 9), HIGH);
    } else if (PWMChannel[i] < 124) {
      // PWM Channel scale from 0 to 100%
      // PWM Direction LOW
      onValue = 0;
      offValue = (uint16_t)((128 - PWMChannel[i]) * hbridgeScaler); // vallue from 0 to 4095
      digitalWrite(HBRIDGE_BEGINPIN + (i - 9), LOW);
    } else {
      // PWM Channel should be OFF
      // PWM Direction doesn't matter
      onValue = 0;
      offValue = 4096;
    }
    Wire.write(onValue & 0x00FF); // On time LOW byte
    Wire.write(onValue >> 8); // On time HIGH byte
    Wire.write(offValue & 0x00FF); // Off time LOW byte.
    Wire.write(offValue >> 8); // Off time HIGH byte
  }
  Wire.endTransmission();
}

// Take i2c address from [2] and request quantity from [3] and send
// over UDP.
void sensorQuery(uint8_t sensorDef[]) {
  Wire.requestFrom(sensorDef[2], sensorDef[3]);
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  while (Wire.available()) {
    Udp.write(Wire.read());
  }
  Udp.endPacket();
}

void sensorWrite(uint8_t sensorValue[], int length) {
  Wire.beginTransmission(sensorValue[2]);
  for (int i = 3; i < length; i++) {
    Wire.write(sensorValue[i]);
  }
  if (Wire.endTransmission() == 0) {
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write("SUCCESS");
    Udp.endPacket();
  } else {
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write("DEV ERR");
    Udp.endPacket();
  }
}
