#!/usr/bin/env python
from udp_motor_controller import *
import time
import random
from Tkinter import *

# How many motors to control:
numberOfMotors = 16

# How frequently to send PWM channel value updates out:
updateRateInMilliseconds = 10

print "[.] Initializing PWM board."
motor_controller = udp_motor_controller(ip = "192.168.1.254", port = 8888, timeout = 0.5)
print "[.] Initialization complete."

oldFreq = 0
motorVars = []

# When we quit we want to make sure all motors are set back to neutral.
def quit():
    root.after_cancel(updatey)
    resetValues()
    updateMotors()
    root.destroy()

def resetValues():
    for x in range(numberOfMotors):
        motorVars[x].set(128)

def updateMotors():
    try:
        motor_controller.updateMotors([motorVars[x].get() for x in range(numberOfMotors)], valueRange=255)
    except:
        print "Couldn't update motors. Trying again in",updateRateInMilliseconds,"ms."
    root.after(updateRateInMilliseconds, updateMotors)
    
def freqUp(newFreq):
    pass
    #x = pwmBoard.setFrequency(int(newFreq))
    #frequencyText.set("Actual PWM frequency is " + str(x) + "Hz.")

def updateCPU(newCPUClock):
    pass
    #pwmBoard.setBoardCPUClock(newCPUClock)
    #freqUp(freqvalue.get())

root = Tk()
root.title("UDP PWM Calibrator/Test")

overallframe = Frame(root)

for x in range(numberOfMotors):
    tempFrame = LabelFrame(overallframe, text = "Motor " + str(x+1))
    motorVars.append(IntVar())
    motorVars[x].set(128)
    tempScroller = Scale(tempFrame, orient=VERTICAL, length=256, from_=255, to=0, variable = motorVars[x])
    tempScroller.pack()
    tempFrame.pack(side=LEFT)

overallframe.pack(expand=1, fill=X)

resetbutton = Button(root, text = "Reset All Throttles to Center Position", command=resetValues)
resetbutton.pack(expand=1, fill=X)

root.protocol("WM_DELETE_WINDOW", quit)

updatey = root.after(50, updateMotors)

mainloop()