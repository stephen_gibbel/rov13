# Just enough stuff in here to test the Arduino-side code.
# Feel free to use it to port your control program over.
import socket
botIP = "192.168.1.10"
botPort = 8888

# New UDP socket (internet/datagram), set timeout on all blocking
# socket operations to 0.5 seconds.
controlSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
controlSocket.settimeout(0.5)

while True:
    dataToSend = raw_input("What do: ")
    # First we send.
    try:
        controlSocket.sendto(dataToSend, (botIP, botPort))
    except:
        print "Failed to send packet."
        
    # Then we look for ack before continuing.
    try:
        returnData, IP = controlSocket.recvfrom(64)
    except:
        print "Failed to receive packet."
    else:
        print "Received", len(returnData), "bytes from", IP, " Some data:\n", returnData
    
