from udp_motor_controller import *
import time

driver = udp_motor_controller(ip = "192.168.1.10", port = 8888, timeout = 0.5)


avgTime = 0

for test in range(0,10):
    ops = 0
    
    beginTime = time.time()
    for x in range(0, 100):
        driver.updateMotors([x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x])
        ops = ops + 1
    for x in range(100, 0, -1):
        driver.updateMotors([x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x])
        ops = ops + 1
    endTime = time.time()

    if avgTime == 0:
        avgTime = endTime - beginTime
    else:
        avgTime = (avgTime + (endTime - beginTime)) / 2

print avgTime / ops
print "done"


while True:
    for x in range(0, 100):
        driver.updateMotors([x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x])
    for x in range(100, 0, -1):
        driver.updateMotors([x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x])