import socket

class udp_motor_controller:
    """Sends motor control signals over UDP to Arduino"""
    
    def __init__(self, ip = "192.168.1.10", port = 8888, timeout = 0.5):
        self.ip = ip
        self.port = port
        self.controlSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Open a UDP socket
        self.controlSocket.settimeout(timeout) # Set blocking socket operation timeout...

    # Take list of motor values (0 to 100%) and scale them to 0-255, convert them to
    # ASCII characters and append them to the string of values to send to the Arduino.
    # Note you can also use any value range you want by passing valueRange = XX to this
    # method.
    #
    # Protocol for Arduino controller requires an "M" at the beginning of any motor
    # control packets, so this is prepended.
    #
    # Looks for confirmation packet from Arduino afterwards to verify operation.
    #
    # Warning: If you send less than 16 values, the remaining channels will be set
    # to 127/center.
    def updateMotors(self, listOfVars, valueRange = 100):
        sendPacket = "M"
        for x in listOfVars:
            scaleVal = int(round(x * 255 / float(valueRange)))
            sendPacket += chr(scaleVal)
        for x in range(17 - len(sendPacket)):
            sendPacket += chr(127)
        try:
            self.controlSocket.sendto(sendPacket, (self.ip, self.port))
        except:
            raise IOError("Failed to send packet")
        else:
            try:
                returnData, fromIP = self.controlSocket.recvfrom(64)
            except:
                raise IOError("Did not receive confirmation")
            else:
                if returnData != "OK":
                    raise IOError("Arduino failed to update PWM values" + "with error code " + returnData)