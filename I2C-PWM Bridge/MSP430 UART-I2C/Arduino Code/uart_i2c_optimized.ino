#include <Wire.h>
#define cmdBufferSize 96


void setup()
{
  pinMode(P1_0, OUTPUT);
  digitalWrite(P1_0, HIGH);
  Serial.begin(115200);
  Serial.println("i2c-uart bridge: dgtech2014");
  Wire.begin();
  digitalWrite(P1_0, LOW);
}

void loop()
{
}


// Grab serial data, one char at a time, and add it to the serialCommand
// char array. Then check what the char actually was: If it was a
// newline, we just got a full command (or a half-command caused by
// an overflow, check flag); if our array index counter has hit maximum,
// we're overflowing, set overflow flag. Otherwise, increment the array index
// counter so the next value goes into the next slot in the array.
//
// Define char array with one extra spot for null termination.
//
// When calling the parseAndRun function, send the current position of
// the array index minus one (since final character is going to be
// newline...) so we know how much of the char array we need.
char serialCommand[cmdBufferSize + 1];
int writeChar = 0;
boolean overflowed = false;
void serialEvent() {
  digitalWrite(P1_0, HIGH);
  while (Serial.available() > 0) {
    char inBuf = Serial.read();
    serialCommand[writeChar] = inBuf;
    if (inBuf == '\n') {
      if (!overflowed) {
        parseAndRun(serialCommand, writeChar - 1);
      } else {
        Serial.print("\nError: Request overflowed serial buffer.\n> ");
        overflowed = false;
        // Reset overflowed flag, because at this point we're
        // about to reset the writeChar to 0 and start grabbing
        // another command.
      }
      memset(serialCommand, 0, cmdBufferSize); // Zero out command.
      writeChar = 0;
    }
    else if (writeChar == cmdBufferSize) {
      overflowed = true;
      memset(serialCommand, 0, cmdBufferSize);
      writeChar = 0;
    } 
    else {
      writeChar++;
    }
  }
  digitalWrite(P1_0, LOW);
}

// Reckless bad-C-code style gigantic block of command parsing fun time!
// At least we have the length of the command from our serial input.
int returnValue = 0;
uint8_t i2cAddress;
uint8_t i2cRequestQty;

uint8_t i2cReadValue;
uint8_t i2cWriteValue;
char i2cReadValueAsString[3];
void parseAndRun(char cmd[], int length) {
  digitalWrite(P1_0, HIGH);
  
  // Disable remote echo.
  //Serial.print(cmd);
  
  // Acquire the destination i2c address.
  i2cAddress = extractByteFromASCIIData(cmd[1], cmd[2]);
  // Yay a switch case for detecting commands.
  switch (cmd[0]) {
    case 'W':
      if (length >= 5) {
        // Iterate through the ASCII pairs that represent
        // the hex values to send to the device.
        // Send the data to the device.
        Wire.beginTransmission(i2cAddress);
        for (int i = 4; i < length; i += 3) {
          // Uncomment these if the line below doesn't work.
          //i2cWriteValue = extractByteFromASCIIData(cmd[i], cmd[i + 1]);
          //Wire.write(i2cWriteValue);
          Wire.write(extractByteFromASCIIData(cmd[i], cmd[i + 1]));
        }
        // If our command was suffixed with a hyphen, keep
        // i2c bus open. Otherwise don't.
        if (cmd[length] == '-') {
          returnValue = Wire.endTransmission(false);
        } else {
          returnValue = Wire.endTransmission(true);
        }
        // Check return value.
        if (returnValue == 0) {
          //Serial.println("Write successful.");
        } else {
          Serial.print("Error: Write failed, code ");
          Serial.print(returnValue);
          Serial.println(".");
        }
      } else {
        Serial.println("Error: Malformed write command.");
      }
      break;
    case 'R':
      if (length >= 5) {
        i2cRequestQty = extractByteFromASCIIData(cmd[4], cmd[5]);
        if (i2cRequestQty > 0) {
          if (cmd[length] == '-') {
            Wire.requestFrom((int)i2cAddress, (int)i2cRequestQty, (int)false);
          } else {
            Wire.requestFrom((int)i2cAddress, (int)i2cRequestQty, (int)true);
          }
          // Read values from wire's buffer. Save them to a uint8_t
          // then convert them to a string representation in base 16
          // and print this out on the screen.
          while (Wire.available() > 0) {
            i2cReadValue = Wire.read();
            itoa(i2cReadValue, i2cReadValueAsString, 16);
            //if (i2cReadValue < 16) Serial.print("0");
            if (i2cReadValue < 16) {
                i2cReadValueAsString[2] = i2cReadValueAsString[1];
                i2cReadValueAsString[1] = i2cReadValueAsString[0];
                i2cReadValueAsString[0] = '0';
            }
            Serial.print(i2cReadValueAsString);
            Serial.print(" ");
          }
          Serial.print("\n");
        } else {
          Serial.println("Error: Requested quantity is zero.");
        }
      } else {
        Serial.println("Error: Malformed read command.");
      }
      break;
    default:
      Serial.println("Command not recognized.");
      Serial.println("Known commands:");
      Serial.println("  * RDD NN");
      Serial.println("      Read NN bytes from i2c device at address DD (hex).");
      Serial.println("  * WDD XX YY ..");
      Serial.println("      Write values XX, YY, etc (hex) to i2c device at address DD (hex).");
      Serial.println("  !! Appending a - to a command will cause the I2C Bridge to send a");
      Serial.println("      RESTART instead of a STOP at the end of the I2C transaction.");
      Serial.println("      This hogs the bus, so use it carefully.");
      Serial.println("        Ex: W40 1F BE -");
      break;
  }
  digitalWrite(P1_0, LOW);
}

uint8_t extractByteFromASCIIData(char hi, char lo) {
  if (isdigit(lo)) {
    lo = lo - '0';
  } else if (islower(lo)) {
    lo = lo - 'a' + 10;
  } else if (isupper(lo)) {
    lo = lo - 'A' + 10;
  }

  if (isdigit(hi)) {
    hi = hi - '0';
  } else if (islower(hi)) {
    hi = hi - 'a' + 10;
  } else if (isupper(hi)) {
    hi = hi - 'A' + 10;
  }
  
  return (hi << 4 | lo);
}
