#include <Wire.h>
#define cmdBufferSize 128

void setup()
{
  pinMode(P1_0, OUTPUT);
  digitalWriteToo(P1_0, HIGH);
  Serial.begin(115200);
  Serial.println("Ready");
  Wire.begin(); //Initialize as a master on the i2c bus.
  digitalWriteToo(P1_0, LOW);
}

void loop()
{
}

// Grab serial data, one char at a time, and add it to the serialCommand
// char array. Then check what the char actually was: If it was a
// newline, we just got a full command (or a half-command caused by
// an overflow, check flag); if our array index counter has hit maximum,
// we're overflowing, set overflow flag. Otherwise, increment the array index
// counter so the next value goes into the next slot in the array.
//
// Define char array with one extra spot for null termination.
//
// When calling the parseAndRun function, send the current position of
// the array index minus one (since final character is going to be
// newline...) so we know how much of the char array we need.
char serialCommand[cmdBufferSize + 1];
int writeChar = 0;
boolean overflowed = false;
void serialEvent() {
  digitalWriteToo(P1_0, HIGH);
  while (Serial.available() > 0) {
    serialCommand[writeChar] = Serial.read();
    if (serialCommand[writeChar] == '\n') {
      if (!overflowed) {
        parseAndRun(serialCommand, writeChar - 1); // pass command buffer to parseAndRun with command length.
      } else {
        Serial.println("EO");
        overflowed = false;
        // Reset overflowed flag, because at this point we're
        // about to reset the writeChar to 0 and start grabbing
        // another command.
      }
      memset(serialCommand, 0, cmdBufferSize); // Zero out command.
      writeChar = 0;
    }
    // Now... if we just wrote to position cmdBuffersize-1, that's the last spot and we've
    // reached our limit. Since this character has already been determined to not be a
    // line break, we know the command is longer than the buffer so we've overflowed.
    else if (writeChar == (cmdBufferSize - 1)) {
      overflowed = true;
      memset(serialCommand, 0, cmdBufferSize);
      writeChar = 0;
    } 
    else {
      writeChar++;
    }
  }
  digitalWriteToo(P1_0, LOW);
}

// Reckless bad-C-code style gigantic block of command parsing fun time!
// At least we have the length of the command from our serial input.
int returnValue = 0;
uint8_t i2cAddress;
uint8_t i2cRequestQty;

uint8_t i2cReadValue;
uint8_t i2cWriteValue;
char i2cReadValueAsString[3];

// Regarding "length": 
//    Values are stored in cmd[] from index 0 to index length.
//    Sooo it's sorta more like length-1. Dat off-by-one risk...
void parseAndRun(char cmd[], int length) {
  digitalWriteToo(P1_0, HIGH);
  
  // Acquire the destination i2c address.
  i2cAddress = extractByteFromASCIIData(cmd[1], cmd[2]);
  // Yay a switch case for detecting commands.
  switch (cmd[0]) {
    case 'W':
      if (length >= 5) {
        // Iterate through the ASCII pairs that represent
        // the hex values to send to the device.
        // Send the data to the device.
        Wire.beginTransmission(i2cAddress);
        // Start from second hex value of each pair, and grab previous
        // (i - 1) value in addition to ith value. This way we don't try
        // to process something that only gives us half a hex pair and
        // end up reading past the end of the string and sending stupid.
        for (int i = 5; i <= length; i += 3) {
          Wire.write(extractByteFromASCIIData(cmd[i - 1], cmd[i]));
        }
        // If our command was suffixed with a hyphen, keep
        // i2c bus open. Otherwise don't.
        if (cmd[length] == '-') {
          returnValue = Wire.endTransmission(false);
        } else {
          returnValue = Wire.endTransmission(true);
        }
        // Check return value.
        if (returnValue == 0) {
          Serial.println("S");
        } else {
          Serial.print("E");
          Serial.println(returnValue);
        }
      } else {
        Serial.println("EM");
      }
      break;
    case 'R':
      if (length >= 5) {
        i2cRequestQty = extractByteFromASCIIData(cmd[4], cmd[5]);
        if (i2cRequestQty > 0) {
          if (cmd[length] == '-') {
            Wire.requestFrom((int)i2cAddress, (int)i2cRequestQty, (int)false);
          } else {
            Wire.requestFrom((int)i2cAddress, (int)i2cRequestQty, (int)true);
          }
          // Read values from wire's buffer. Save them to a uint8_t
          // then convert them to a string representation in base 16
          // and print this out on the screen.
          while (Wire.available() > 0) {
            i2cReadValue = Wire.read();
            itoa(i2cReadValue, i2cReadValueAsString, 16);
            //if (i2cReadValue < 16) Serial.print("0");
            if (i2cReadValue < 16) {
                i2cReadValueAsString[2] = i2cReadValueAsString[1];
                i2cReadValueAsString[1] = i2cReadValueAsString[0];
                i2cReadValueAsString[0] = '0';
            }
            Serial.print(i2cReadValueAsString);
            Serial.print(" ");
          }
          Serial.print("\n");
        } else {
          Serial.println("EZ");
        }
      } else {
        Serial.println("EM");
      }
      break;
    default:
      Serial.println(F("Command not recognized."));
      Serial.println(F("Known commands:"));
      Serial.println(F("  * RDD NN"));
      Serial.println(F("      Read NN bytes (quantity in hex) from i2c device at address DD (hex)."));
      Serial.println(F("  * WDD XX YY .."));
      Serial.println(F("      Write values XX, YY, etc (hex) to i2c device at address DD (hex)."));
      Serial.println(F("Appending a - to a command to end with restart condition and leave bus open."));
      break;
  }
  digitalWriteToo(P1_0, LOW);
}

uint8_t extractByteFromASCIIData(char hi, char lo) {
  if (isdigit(lo)) {
    lo = lo - '0';
  } else if (islower(lo)) {
    lo = lo - 'a' + 10;
  } else if (isupper(lo)) {
    lo = lo - 'A' + 10;
  }

  if (isdigit(hi)) {
    hi = hi - '0';
  } else if (islower(hi)) {
    hi = hi - 'a' + 10;
  } else if (isupper(hi)) {
    hi = hi - 'A' + 10;
  }
  
  return (hi << 4 | lo);
}

// Faster way of setting pin values. This is for MSP430G2553 specifically:
// Pin to port mapping:
//   2 - Port 1 - Pin 0
//   3 - Port 1 - Pin 1
//   4 - Port 1 - Pin 2
//   5 - Port 1 - Pin 3
//   6 - Port 1 - Pin 4
//   7 - Port 1 - Pin 5
//  14 - Port 1 - Pin 6
//  15 - Port 1 - Pin 7

//   8 - Port 2 - Pin 0
//   9 - Port 2 - Pin 1
//  10 - Port 2 - Pin 2
//  11 - Port 2 - Pin 3
//  12 - Port 2 - Pin 4
//  13 - Port 2 - Pin 5
//  19 - Port 2 - Pin 6
//  18 - Port 2 - Pin 7
void digitalWriteToo(uint8_t pinNumber, uint8_t highOrLow) {
  if (highOrLow == HIGH) {
    switch (pinNumber) {
      case 2:
        P1OUT |= B00000001;
        break;
      case 3:
        P1OUT |= B00000010;
        break;
      case 4:
        P1OUT |= B00000100;
        break;
      case 5:
        P1OUT |= B00001000;
        break;
      case 6:
        P1OUT |= B00010000;
        break;
      case 7:
        P1OUT |= B00100000;
        break;
      case 14:
        P1OUT |= B01000000;
        break;
      case 15:
        P1OUT |= B10000000;
        break;
        
      case 8:
        P2OUT |= B00000001;
        break;
      case 9:
        P2OUT |= B00000010;
        break;
      case 10:
        P2OUT |= B00000100;
        break;
      case 11:
        P2OUT |= B00001000;
        break;
      case 12:
        P2OUT |= B00010000;
        break;
      case 13:
        P2OUT |= B00100000;
        break;
      case 19:
        P2OUT |= B01000000;
        break;
      case 18:
        P2OUT |= B10000000;
        break;
    }
  } else if (highOrLow == LOW) {
    switch (pinNumber) {
      case 2:
        P1OUT &= B11111110;
        break;
      case 3:
        P1OUT &= B11111101;
        break;
      case 4:
        P1OUT &= B11111011;
        break;
      case 5:
        P1OUT &= B11110111;
        break;
      case 6:
        P1OUT &= B11101111;
        break;
      case 7:
        P1OUT &= B11011111;
        break;
      case 14:
        P1OUT &= B10111111;
        break;
      case 15:
        P1OUT &= B01111111;
        break;
        
      case 8:
        P2OUT &= B11111110;
        break;
      case 9:
        P2OUT &= B11111101;
        break;
      case 10:
        P2OUT &= B11111011;
        break;
      case 11:
        P2OUT &= B11110111;
        break;
      case 12:
        P2OUT &= B11101111;
        break;
      case 13:
        P2OUT &= B11011111;
        break;
      case 19:
        P2OUT &= B10111111;
        break;
      case 18:
        P2OUT &= B01111111;
        break;
    }
  }
}
