#!/usr/bin/env python
from pwmController import *
from hk_usb_io import *
import time

def timeThisFunction(functionToTime, iterations = 50):
    timeStats = []
    for i in range(iterations):
        beginTime = time.time()
        functionToTime()
        timeStats.append(time.time() - beginTime)
    return sum(timeStats) / len(timeStats)

def rawWrite(io, dangerZone = False):
    i2c_start(io, I2C_START_CMD)
    i2c_write(io, (0x40 << 1 | I2C_WRITE_CMD))
    if not dangerZone:
        while (i2c_slave_ack(io)):
            time.sleep(0.005)
    i2c_write(io, 0x06)
    if not dangerZone:
        while (i2c_slave_ack(io)):
            time.sleep(0.005)
    i2c_write(io, 0x00)
    if not dangerZone:
        while (i2c_slave_ack(io)):
            time.sleep(0.005)
    i2c_write(io, 0x00)
    if not dangerZone:
        while (i2c_slave_ack(io)):
            time.sleep(0.005)
    i2c_write(io, 0x00)
    if not dangerZone:
        while (i2c_slave_ack(io)):
            time.sleep(0.005)
    i2c_write(io, 0x08)
    if not dangerZone:
        while (i2c_slave_ack(io)):
            time.sleep(0.005)
    i2c_stop(io)

usbIOBoard = init()
# Don't use this code in production. rom_version likes to randomly
# time out and throw exceptions.
#print "-------------------------------------------------"
#print "HK IO Board:"
#print module_version() + ", ROM: " + rom_version(usbIOBoard)
#print "-------------------------------------------------"
print "-------------------------------------------------"

print "[.] Initializing PCA9685 board in regular mode."
pwmBoard = pwmController(usbIOBoard, pwmBoardAddress = 0x40, dangerZone = False)
pwmBoard.initializeDevice()
print "[.] Initialization complete."
print
print "Timing function setPWMByDutyCycle..."
print "Took", timeThisFunction(lambda: pwmBoard.setPWMByDutyCycle(0, 50),
                               iterations = 100), "seconds average."
print
print "Timing raw i2c commands..."
print "Took", timeThisFunction(lambda: rawWrite(usbIOBoard, dangerZone = False),
                               iterations = 100), "seconds average."
print "-------------------------------------------------"
print "[.] Initializing PCA9685 board in dangerZone mode."
pwmBoard = pwmController(usbIOBoard, pwmBoardAddress = 0x40, dangerZone = True)
pwmBoard.initializeDevice()
print "[.] Initialization complete."
print
print "Timing function setPWMByDutyCycle..."
print "Took", timeThisFunction(lambda: pwmBoard.setPWMByDutyCycle(0, 50),
                               iterations = 100), "seconds average."
print
print "Timing raw i2c commands..."
print "Took", timeThisFunction(lambda: rawWrite(usbIOBoard, dangerZone = True),
                               iterations = 100), "seconds average."
print "-------------------------------------------------"