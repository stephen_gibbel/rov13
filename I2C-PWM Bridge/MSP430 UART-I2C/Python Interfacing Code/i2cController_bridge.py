#!/usr/bin/env python
# Devon Goode 2014.
#
# i2c-serial bridge by Devon Goode, this interface code
# is intended for use by higher level code such as 
# pwmController.py.
#
# Requires pyserial library for serial communications.
import serial

class i2cController(object):
    def __init__(self, serialPort, baud = 115200):
        self.serialPort = serialPort
        self.serialDevice = serial.Serial(serialPort, baud, timeout = 1)

    # Write to device:
    #   Send in serial:
    #           WDD RR XX YY ZZ...
    # Where DD is device address, RR is register address, XX YY ZZ are values
    # to write. All in hex.
    #
    # valuesToWrite should be a list.
    #
    # Return True or False depending on success. Future revisions will return
    # more information to reveal types of failure. Oooh, or we could do
    # some error throwing, that might be even more fun!
    def writeRegisters(self, i2cAddress, startingRegisterAddress, valuesToWrite):
        # Stick all the values into a string. Formatted as two-digit hex values.
        allTheValues = ""
        for value in valuesToWrite:
            allTheValues = allTheValues + '{:02X}'.format(value) + " "
            
        # Send serial command. Chop off the final space in allTheValues.
        self.serialDevice.write("W" +
                                '{:02X}'.format(i2cAddress) + " " +
                                '{:02X}'.format(startingRegisterAddress) + " " +
                                allTheValues[:-1] + "\n")
        
        returnValueString = self.serialDevice.readline().strip()
    
        if returnValueString[0] == "E":
            raise IOError("Error number " + returnValueString[1] + ".")
            return False
        else:
            return True
        
    # Read from device:
    #   Send in serial:
    #           WDD RR
    # Where DD is device address, RR is register to start reading from.
    #   Now send:
    #           RDD NN
    # Where DD is device address, NN is number of registers to read.
    #
    # Values come in as space delimited two-digit hex values.
    #
    # Returns a single-item list containing zero if reading fails.
    def readRegisters(self, i2cAddress, startingRegisterAddress, numToRead):
        self.serialDevice.write("W" +
                                '{:02X}'.format(i2cAddress) + " " +
                                '{:02X}'.format(startingRegisterAddress) + "\n")
        
        writeSuccessString = self.serialDevice.readline().strip()
        if writeSuccessString[0] == "E":
            raise IOError("Error number " + writeSuccessString[1] + ".")
        
        self.serialDevice.write("R" +
                                '{:02X}'.format(i2cAddress) + " " +
                                '{:02X}'.format(numToRead) + "\n")
        
        # Grab data from serial board. Strip any spaces and/or newlines at end.
        returnValueString = self.serialDevice.readline().strip()
        #print "Device replied:" + returnValueString
        
        # Scary thing below iterates through returnValueString, character by character, extracting two
        # characters at a time and skipping the third, converts them to integers from base-16 (hex)
        # string representations, and sticks them all in a list.
        if returnValueString[0] == "E":
            raise IOError("Error number " + writeSuccessString[1] + ".")
            return [0]
        else:
            return [int(returnValueString[i:i+2], 16) for i in xrange(0, len(returnValueString), 3)]
    
    # Send W00 06, some kind of magical i2c reset thingy.
    # Return True if success is reported, else return False.
    def i2cReset(self):
        self.serialDevice.write("W00 06\n")
        returnValueString = self.serialDevice.readline().strip()
        if returnValueString == "S":
            return True
        else:
            return False