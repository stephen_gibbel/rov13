#!/usr/bin/env python
from i2cController_bridge import *
import time

# Devon Goode 2013. pwmController class to control Adafruit PCA9685-based
# 16-channel PWM board. This library is designed to be used in conjunction with
# the i2c-uart bridge to control the PWM board. Has most basic features,
# including setting the PWM based on duty cycle desired or based on RC throttle/
# servo 1ms-to-2ms control range. Automatically converts from desired servo
# "percentage" to a value between 1ms and 2ms regardless of board output frequency.
#
# Future, probably not going to happen features include ability to use external
# time sources for the board using EXTCLK setting, options to tweak all MODE1 and
# MODE2 settings and treat them as a dictionary of booleans...
#
class pwmController(i2cController):
    def __init__(self, serialPort, baud = 115200, pwmBoardAddress = 0x40):
        super(pwmController, self).__init__(serialPort, baud)
        self.pwmBoardAddress = pwmBoardAddress
        self.pwmBoardClockrate = 25000000
        self.pwmOutputClockrate = 0
        self.MODE1 = 0x00
        self.MODE2 = 0x00
        self.initialized = False # Since it was just created, it hasn't been initialized yet.

    # This one does what it says on the tin.
    def initializeDevice(self):
        if not self.initialized:
            self.softwareReset() # Get device into known state by resetting it.
            # Grab values from MODE1 and MODE2 registers.
            # Then grab prescale value to find output
            # PWM frequency from that.
            self.MODE1 = self.readRegisters(self.pwmBoardAddress, 0x00, 1)[0]
            self.MODE2 = self.readRegisters(self.pwmBoardAddress, 0x01, 1)[0]
            prescaleValue = self.readRegisters(self.pwmBoardAddress, 0xFE, 1)[0]
            self.pwmOutputClockrate = self.pwmBoardClockrate / (4096. * (prescaleValue + 1))
            # Set AI (bit 5 of the mode 1 register).
            self.MODE1 = self.MODE1 | 0b00100000
            # Save MODE1's new settings back to the board.
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
            # Now wake board up.
            self.wakeUp()
            self.initialized = True
        
    # Get the prescale value from the board, calculate the actual clock (update) rate
    # and return it.
    def getFrequency(self):
        prescaleValue = self.readRegisters(self.pwmBoardAddress, 0xFE, 1)[0]
        self.pwmOutputClockrate = self.pwmBoardClockrate / (4096. * (prescaleValue + 1))
        return self.pwmOutputClockrate
    
    # Take a frequency, turn it into a prescale value. Then write this to the board.
    # Then use that prescale value and recalculate the actual frequency we just
    # set the board to, and return that value.
    def setFrequency(self, freq):
        self.goToSleep()
        prescaleValue = int(self.pwmBoardClockrate / (4096 * freq)) - 1
        if prescaleValue < 3:
            raise ValueError("Prescale value ended up less than 3. Try a lower PWM frequency.")
        elif prescaleValue > 255:
            raise ValueError("Prescale value over 255. Try a higher PWM frequency.")
        self.writeRegisters(self.pwmBoardAddress, 0xFE, [prescaleValue])
        self.wakeUp()
        # Recalculate output clockrate based on derived prescale value.
        # Rounding has probably changed the actual clockrate slightly.
        # So we will return it.
        self.pwmOutputClockrate = self.pwmBoardClockrate / (4096. * (prescaleValue + 1))
        return self.pwmOutputClockrate

    # Make board go to sleep. Used for setFrequency. This pauses operation
    # but saves PWM settings.
    def goToSleep(self):
        self.MODE1 = self.MODE1 | 0b00010000
        self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
    
    # Wake up the board. Resume where we left off!
    def wakeUp(self):
        self.MODE1 = self.readRegisters(self.pwmBoardAddress, 0x00, 1)[0]
        # Check if bit 7 of the MODE1 register (reset bit) is set.
        # If so, clear bit 4 (sleep), wait 1ms, "set" bit 7 (which magically clears reset)
        # Otherwise just clear sleep (bit 4).
        if self.MODE1 & (1 << 7) != 0:
            self.MODE1 = self.MODE1 & 0b11101111
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1 & 0b01111111])
            time.sleep(0.001)
            self.MODE1 = self.MODE1 | 0b10000000
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
        else:
            # Clear bit 4, aka take board out of sleep mode.
            self.MODE1 = self.MODE1 & 0b11101111
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
        
    # Perform software reset as explained in section 7.6 of datasheet.
    # Icky i2c features used directly in here. Oh well. i2cController
    # is supposed to be general purpose and this is proprietary to
    # the PCA chip.
    def softwareReset(self):
        self.i2cReset()
# -------------------------------------- PWM Stuff Below --------------------------------------------
# Duty cycle percentage methods below.

    # Apologies in advance for Java-esque method naming...
    # This monster returns a list formatted as required for
    # writing to the registers of the PCA chip.
    #   ON_LOW_BIT ON_HIGH_BIT OFF_LOW_BIT OFF_HIGH_BIT
    def getTimerValuesForDutyCyclePWM(self, dutyCycle, offset = 0):
        if dutyCycle >= 0 and dutyCycle <= 100:
            onValue = offset
            onDuration = int(dutyCycle * 40.95)
            if onDuration == 0:
                onValue  = 0b0000000000000000
                offValue = 0b0001000000000000
            elif onDuration == 4095:
                onValue  = 0b0001000000000000
                offValue = 0b0000000000000000
            else:
                if onValue + onDuration < 4095:
                    offValue = onValue + onDuration
                else:
                    offValue = (onValue + onDuration) - 4095
        else:
            raise ValueError("Duty cycle out of range.")
        offValue = divmod(offValue, 0x100)
        onValue = divmod(onValue, 0x100)
        return [onValue[1], onValue[0], offValue[1], offValue[0]]
        
    # Takes PWM Channel, duty cycle from 0 to 100, and (optional) offset in 4096-step-counter-values.
    # Bonus: If dutyCycle is 0 or 100, uses board-specific setting to turn channel full-on
    #        or full-off!
    def setPWMByDutyCycle(self, PWMChannel, dutyCycle, offset = 0):
        if (PWMChannel < 0 or PWMChannel > 15):
            raise ValueError("PWM channel out of range.")
        if (dutyCycle < 0 or dutyCycle > 100):
            raise ValueError("Duty cycle out of range.")
        if (offset < 0 or offset > 4095):
            raise ValueError("Offset out of range.")
        valueForRegisters = self.getTimerValuesForDutyCyclePWM(dutyCycle, offset)
        # Calculate start register.
        startRegister = 4 * PWMChannel + 6
        self.writeRegisters(self.pwmBoardAddress, startRegister, valueForRegisters)
        
    # Set all PWM channels in one fell swoop.
    def setAllPWMChannelsByDutyCycle(self, dutyCycles):
        print dutyCycles
        if len(dutyCycles) > 8:
            raise ValueError("Too many channels. i2c bridge can only handle 6 at the moment...")
        onAndOffValues = map(self.getTimerValuesForDutyCyclePWM, dutyCycles)
        startRegister = 0x06 # Start at first PWM channel.
        onAndOffValues = [item for sublist in onAndOffValues for item in sublist]
        self.writeRegisters(self.pwmBoardAddress, startRegister, onAndOffValues)
        
#--------------------------------------------------------------------------------------------------------
# Servo style PWM methods.

    def getTimerValuesForServoPWM(self, throttlePercentage):
        if throttlePercentage >= 0 and throttlePercentage <= 100:
            ticksIn1ms = self.pwmOutputClockrate * 4.096
            if ticksIn1ms < 100:
                print "[!] Warning: Tick resolution too small for accurate servo-style PWM. Try higher PWM frequency."
            onValue = 0
            offValue = int(ticksIn1ms + throttlePercentage / 100. * ticksIn1ms)
        else:
            raise ValueError("Throttle percentage out of range.")
        # Now we found offValue, make sure it's reasonable, return registers.
        if offValue < 4096:
            onValue = divmod(onValue, 0x100)
            offValue = divmod(offValue, 0x100)
        else:
            raise ValueError("Throttle percentage impossible with current PWM frequency setting.")
        return [onValue[1], onValue[0], offValue[1], offValue[0]]

    # Takes PWM Channel, throttle percentage from 0 to 100.
    def setPWMLikeServo(self, PWMChannel, throttlePercentage):
        if (PWMChannel < 0 or PWMChannel > 15):
            raise ValueError("PWM channel out of range.")
        if (throttlePercentage < 0 or throttlePercentage > 100):
            raise ValueError("Throttle percentage out of range.")
        # Find how many timer-ticks to get 1ms.
        # Do 1ms-worth-of-ticks + 1ms worth-of-ticks * throttlePercentage/100.
        # Ticks per millisecond:
        #   - There are pwmOutputClockrate PWM refreshes per second
        #   - There are 4096 ticks per iteration.
        #   - There are pwmOutputClockrate iterations per second.
        #   - pwmOutputClockrate iterations per second * 4096 ticks per iteration = # of ticks per second
        #   - ticks per second / 1000 = ticks per millisecond
        ticksIn1ms = self.pwmOutputClockrate * 4.096
        if ticksIn1ms < 100:
            print "[!] Warning: Tick resolution too small for accurate servo-style PWM. Try higher PWM frequency."
        onValue = 0
        offValue = int(ticksIn1ms + throttlePercentage / 100. * ticksIn1ms)
        if offValue < 4096:
            startRegister = 4 * PWMChannel + 6
            onValue = divmod(onValue, 0x100)
            offValue = divmod(offValue, 0x100)
            self.writeRegisters(self.pwmBoardAddress, startRegister,
                                        [onValue[1], onValue[0], offValue[1], offValue[0]])
        else:
            raise ValueError("PWM value outside allowable range. Try lower PWM frequency.")
        
    def setAllPWMChannelsLikeServo(self, throttlePercentages):
        if len(throttlePercentages) > 8:
            raise ValueError("Too many channels given. i2c bridge can only handle 6 at the moment...")
        onAndOffValues = map(self.getTimerValuesForServoPWM, throttlePercentages)
        startRegister = 0x06 # Start at first PWM channel.
        onAndOffValues = [item for sublist in onAndOffValues for item in sublist]
        self.writeRegisters(self.pwmBoardAddress, startRegister, onAndOffValues)