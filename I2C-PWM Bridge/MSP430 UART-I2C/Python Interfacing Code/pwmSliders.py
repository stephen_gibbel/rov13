#!/usr/bin/env python
from pwmController2 import *
import time
import random
from Tkinter import *

# How many motors to control:
numberOfMotors = 8

# Default PWM output frequency in Hz:
defaultFrequency = 100

# How frequently to send PWM channel value updates out:
updateRateInMilliseconds = 10

# Default CPU clock rate assumed for PCA9685 PWM board:
defaultCPUClockMHz = 26.75

print "[.] Initializing PWM board."
pwmBoard = pwmController2('/dev/ttyUSB0', pwmBoardAddress = 0x40, pwmBoardClockrate = defaultCPUClockMHz*10**6)
pwmBoard.initializeDevice()
print "[.] Initialization complete."

oldFreq = 0
motorVars = []

# When we quit we want to make sure all motors are set back to neutral.
def quit():
    root.after_cancel(updatey)
    resetValues()
    updateMotors()
    root.destroy()

def resetValues():
    for x in range(numberOfMotors):
        motorVars[x].set(50)

def updateMotors():
    global oldFreq
    if freqvalue.get() != oldFreq:
        freqUp(freqvalue.get())
        oldFreq = freqvalue.get()
    #pwmBoard.setPWMByDutyCycle(0, [motorVars[x].get() for x in range(numberOfMotors)])
    pwmBoard.setPWMByDutyLikeServo(0, [motorVars[x].get() for x in range(numberOfMotors)])
    root.after(updateRateInMilliseconds, updateMotors)
    
def freqUp(newFreq):
    x = pwmBoard.setFrequency(int(newFreq))
    frequencyText.set("Actual PWM frequency is " + str(x) + "Hz.")

def updateCPU(newCPUClock):
    pwmBoard.setBoardCPUClock(newCPUClock)
    freqUp(freqvalue.get())

root = Tk()
root.title("PWM Board Calibration Test v2.0")

BoardCPUFrequencyFrame = LabelFrame(root, text = "Assumed CPU Frequency (MHz)", fg = "#000088")
BoardCPUFrequencyMHz = DoubleVar()
BoardCPUFrequencyMHz.set(defaultCPUClockMHz)
BoardCPUFrequencyScroller = Scale(BoardCPUFrequencyFrame, resolution = 0.05, orient=HORIZONTAL, from_=23.0, to=27.0, variable = BoardCPUFrequencyMHz, command = updateCPU)
BoardCPUFrequencyScroller.pack(expand=1, fill=X)
BoardCPUFrequencyFrame.pack(expand=1, fill=X)

overallframe = Frame(root)

for x in range(numberOfMotors):
    tempFrame = LabelFrame(overallframe, text = "Motor " + str(x+1))
    motorVars.append(IntVar())
    motorVars[x].set(50)
    tempScroller = Scale(tempFrame, orient=VERTICAL, length=256, from_=100, to=0, variable = motorVars[x])
    tempScroller.pack()
    tempFrame.pack(side=LEFT)

freqframe = LabelFrame(overallframe, text = "PWM Frequency (Hz)", fg = "#00AA00")
freqvalue = IntVar()
freqvalue.set(defaultFrequency)
freqscroller = Scale(freqframe, orient=VERTICAL, length=256, from_=30, to =400, variable = freqvalue, fg = "#006600")
freqscroller.pack()
freqframe.pack(side=RIGHT)

overallframe.pack(expand=1, fill=X)

resetbutton = Button(root, text = "Reset All Throttles to Center Position", command=resetValues)
resetbutton.pack(expand=1, fill=X)

frequencyText = StringVar()
frequencyText.set("Actual PWM frequency is...")
frequencyTextLabel = Label(root, textvariable = frequencyText)
frequencyTextLabel.pack(expand=1, fill=X)

root.protocol("WM_DELETE_WINDOW", quit)

updatey = root.after(50, updateMotors)

mainloop()