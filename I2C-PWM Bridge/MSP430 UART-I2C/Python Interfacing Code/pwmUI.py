#!/usr/bin/env python
from pwmController2 import *
import time
import random
# This mess is turning into more of a debug staging ground for
# trying to figure out what is wrong with the i2c bridge and/or
# the PCA PWM board regarding fast changes. This code makes no 
# sense and will probably keep changing.

print "[.] Initializing PCA9685 board."
pwmBoard = pwmController2('/dev/ttyUSB0', pwmBoardAddress = 0x40)
pwmBoard.initializeDevice()
print "[.] Initialization complete."

beginTime = time.time()
while True:
    a = random.randint(0, 4)
    if a == 0:
        pwmBoard.getFrequency()
    if a == 1:
        pwmBoard.setFrequency(random.randint(35,200))
    if a == 2:
        pwmBoard.goToSleep()
    if a == 3:
        pwmBoard.wakeUp()
    if a == 4:
        pwmBoard.setPWMByDutyCycle(0, [random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100)])
        pwmBoard.setPWMByDutyCycle(8, [random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100), random.randint(0,100)])
    #time.sleep(0.005)
    print time.time() - beginTime
    #print "[.] First 8 channels fade-in-out simultaneously."
    #startTime = time.time()
    #for x in range(0, 100):
        #out = 100 * ((x / 100.) ** 3) # Gamma correction.
        #pwmBoard.setPWMByDutyCycle(0, [out, out, out, out, out, out, out, out])

    #for x in range(100, 0, -1):
        #out = 100 * ((x / 100.) ** 3) # Gamma correction.
        #pwmBoard.setPWMByDutyCycle(0, [out, out, out, out, out, out, out, out])
    #endTime = time.time()
    #print "[!] Took ",endTime - startTime, "seconds."

#print "[!] Done!"