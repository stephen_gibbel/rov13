EESchema Schematic File Version 2  date Sun 23 Feb 2014 11:04:26 AM PST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MSP430G2231
LIBS:bridgeboard-cache
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "23 feb 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MSP430G2553 U2
U 1 1 52DF2648
P 4250 2650
F 0 "U2" H 4250 1850 50  0000 C CNN
F 1 "MSP430G2553" H 4250 3100 50  0000 C CNN
F 2 "MODULE" H 4250 2650 50  0001 C CNN
F 3 "DOCUMENTATION" H 4250 2650 50  0001 C CNN
	1    4250 2650
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 52DF2327
P 5350 2400
F 0 "R1" V 5430 2400 40  0000 C CNN
F 1 "10k" V 5357 2401 40  0000 C CNN
F 2 "~" V 5280 2400 30  0000 C CNN
F 3 "~" H 5350 2400 30  0000 C CNN
	1    5350 2400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 52DF2343
P 5150 2550
F 0 "#PWR01" H 5150 2550 30  0001 C CNN
F 1 "GND" H 5150 2480 30  0001 C CNN
F 2 "" H 5150 2550 60  0000 C CNN
F 3 "" H 5150 2550 60  0000 C CNN
	1    5150 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2350 5150 2350
Wire Wire Line
	5150 2350 5150 2550
Wire Wire Line
	5350 2650 5350 2750
Wire Wire Line
	5350 2750 5000 2750
$Comp
L LED D1
U 1 1 52DF23A6
P 3200 2450
F 0 "D1" H 3200 2550 50  0000 C CNN
F 1 "LED" H 3200 2350 50  0000 C CNN
F 2 "~" H 3200 2450 60  0000 C CNN
F 3 "~" H 3200 2450 60  0000 C CNN
	1    3200 2450
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR02
U 1 1 52DF23B5
P 2450 2500
F 0 "#PWR02" H 2450 2500 30  0001 C CNN
F 1 "GND" H 2450 2430 30  0001 C CNN
F 2 "" H 2450 2500 60  0000 C CNN
F 3 "" H 2450 2500 60  0000 C CNN
	1    2450 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2450 3500 2450
Wire Wire Line
	3500 2550 3350 2550
Wire Wire Line
	3350 2550 3350 2650
Wire Wire Line
	3350 2650 2550 2650
Wire Wire Line
	3500 2650 3400 2650
Wire Wire Line
	3400 2650 3400 2750
Text Label 2650 2650 0    60   ~ 0
RXD_3v3
$Comp
L R R4
U 1 1 52DF2515
P 2550 2900
F 0 "R4" V 2630 2900 40  0000 C CNN
F 1 "2k2" V 2557 2901 40  0000 C CNN
F 2 "~" V 2480 2900 30  0000 C CNN
F 3 "~" H 2550 2900 30  0000 C CNN
	1    2550 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 52DF2524
P 2550 3150
F 0 "#PWR03" H 2550 3150 30  0001 C CNN
F 1 "GND" H 2550 3080 30  0001 C CNN
F 2 "" H 2550 3150 60  0000 C CNN
F 3 "" H 2550 3150 60  0000 C CNN
	1    2550 3150
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 52DF2541
P 2300 2650
F 0 "R3" V 2380 2650 40  0000 C CNN
F 1 "1k" V 2307 2651 40  0000 C CNN
F 2 "~" V 2230 2650 30  0000 C CNN
F 3 "~" H 2300 2650 30  0000 C CNN
	1    2300 2650
	0    -1   -1   0   
$EndComp
Text GLabel 2050 2650 0    60   Input ~ 0
RXD
Text GLabel 2950 2750 0    60   Input ~ 0
TXD
Wire Wire Line
	3400 2750 2950 2750
NoConn ~ 3500 2750
NoConn ~ 3500 2850
NoConn ~ 3500 2950
NoConn ~ 3500 3050
NoConn ~ 3500 3150
NoConn ~ 3500 3250
$Comp
L R R2
U 1 1 52DF26D3
P 2750 2450
F 0 "R2" V 2830 2450 40  0000 C CNN
F 1 "150" V 2757 2451 40  0000 C CNN
F 2 "~" V 2680 2450 30  0000 C CNN
F 3 "~" H 2750 2450 30  0000 C CNN
	1    2750 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	2450 2500 2450 2450
Wire Wire Line
	2450 2450 2500 2450
NoConn ~ 5000 2450
NoConn ~ 5000 2550
NoConn ~ 5000 2650
NoConn ~ 5000 3050
NoConn ~ 5000 3150
NoConn ~ 5000 3250
Text GLabel 5350 2150 1    60   Input ~ 0
VCC
Wire Wire Line
	3500 2350 3400 2350
Wire Wire Line
	3400 2350 3400 2200
Text GLabel 3400 2200 1    60   Input ~ 0
VCC
$Comp
L LP2950 U1
U 1 1 52DF2975
P 4250 1450
F 0 "U1" H 4400 1254 60  0000 C CNN
F 1 "LP2950" H 4250 1650 60  0000 C CNN
F 2 "~" H 4250 1450 60  0000 C CNN
F 3 "~" H 4250 1450 60  0000 C CNN
	1    4250 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 52DF29D8
P 4250 1700
F 0 "#PWR04" H 4250 1700 30  0001 C CNN
F 1 "GND" H 4250 1630 30  0001 C CNN
F 2 "" H 4250 1700 60  0000 C CNN
F 3 "" H 4250 1700 60  0000 C CNN
	1    4250 1700
	1    0    0    -1  
$EndComp
Text GLabel 4650 1400 2    60   Input ~ 0
VCC
Text GLabel 3850 1400 0    60   Input ~ 0
VIN
Text GLabel 6100 2700 1    60   Input ~ 0
SDA
Text GLabel 6650 2800 1    60   Input ~ 0
SCL
Wire Wire Line
	5000 2850 5600 2850
Wire Wire Line
	5000 2950 6150 2950
Text Label 5050 2750 0    60   ~ 0
RESET
Text Label 5050 2350 0    60   ~ 0
GND
Text Label 3400 2450 0    60   ~ 0
LED
Text GLabel 5950 1100 0    60   Input ~ 0
SDA
Text GLabel 6250 1000 0    60   Input ~ 0
SCL
$Comp
L CONN_4 P1
U 1 1 52DF2E27
P 6750 1850
F 0 "P1" V 6700 1850 50  0000 C CNN
F 1 "MALE_PINS" V 6800 1850 50  0000 C CNN
F 2 "~" H 6750 1850 60  0000 C CNN
F 3 "~" H 6750 1850 60  0000 C CNN
	1    6750 1850
	1    0    0    -1  
$EndComp
Text GLabel 6400 1700 0    60   Input ~ 0
VIN
Text GLabel 6100 1800 0    60   Input ~ 0
TXD
Text GLabel 6400 1900 0    60   Input ~ 0
RXD
Text GLabel 6100 2000 0    60   Input ~ 0
GND
Wire Wire Line
	6100 2000 6400 2000
Wire Wire Line
	6400 1800 6100 1800
$Comp
L IRF540N Q1
U 1 1 52DF2FA9
P 5800 2750
F 0 "Q1" H 5800 2602 40  0000 R CNN
F 1 "2N7000" H 5800 2899 40  0000 R CNN
F 2 "TO92" H 5621 2851 29  0000 C CNN
F 3 "~" H 5800 2750 60  0000 C CNN
	1    5800 2750
	0    1    1    0   
$EndComp
$Comp
L IRF540N Q2
U 1 1 52DF2FBA
P 6350 2850
F 0 "Q2" H 6350 2702 40  0000 R CNN
F 1 "2N7000" H 6350 2999 40  0000 R CNN
F 2 "TO92" H 6171 2951 29  0000 C CNN
F 3 "~" H 6350 2850 60  0000 C CNN
	1    6350 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 2150 6300 2150
Wire Wire Line
	5750 2150 5750 2550
Wire Wire Line
	6300 2150 6300 2650
Connection ~ 5750 2150
Wire Wire Line
	6000 2850 6100 2850
Wire Wire Line
	6100 2850 6100 2700
Wire Wire Line
	6550 2950 6650 2950
Wire Wire Line
	6650 2950 6650 2800
$Comp
L C C1
U 1 1 52DF3408
P 4250 2050
F 0 "C1" H 4250 2150 40  0000 L CNN
F 1 "0.1u" H 4256 1965 40  0000 L CNN
F 2 "~" H 4288 1900 30  0000 C CNN
F 3 "~" H 4250 2050 60  0000 C CNN
	1    4250 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 2050 3500 2050
Wire Wire Line
	3500 2050 3500 2350
Wire Wire Line
	4450 2050 5000 2050
Wire Wire Line
	5000 2050 5000 2350
Text GLabel 6250 800  0    60   Input ~ 0
GND
$Comp
L R R5
U 1 1 530A42B2
P 6000 3350
F 0 "R5" V 6080 3350 40  0000 C CNN
F 1 "10k" V 6007 3351 40  0000 C CNN
F 2 "~" V 5930 3350 30  0000 C CNN
F 3 "~" H 6000 3350 30  0000 C CNN
	1    6000 3350
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 530A42C1
P 6550 3350
F 0 "R6" V 6630 3350 40  0000 C CNN
F 1 "10k" V 6557 3351 40  0000 C CNN
F 2 "~" V 6480 3350 30  0000 C CNN
F 3 "~" H 6550 3350 30  0000 C CNN
	1    6550 3350
	1    0    0    -1  
$EndComp
Text GLabel 6300 3700 3    60   Input ~ 0
VIN
Wire Wire Line
	6000 3600 6550 3600
Wire Wire Line
	6300 3600 6300 3700
Connection ~ 6300 3600
Wire Wire Line
	6550 3100 6550 2950
Wire Wire Line
	6000 3100 6000 2850
$Comp
L CONN_5 P2
U 1 1 530A440A
P 6750 1000
F 0 "P2" V 6700 1000 50  0000 C CNN
F 1 "FEMALE_PINS" V 6800 1000 50  0000 C CNN
F 2 "~" H 6750 1000 60  0000 C CNN
F 3 "~" H 6750 1000 60  0000 C CNN
	1    6750 1000
	1    0    0    -1  
$EndComp
Text GLabel 5950 900  0    60   Input ~ 0
OE
Text GLabel 6250 1200 0    60   Input ~ 0
VIN
Wire Wire Line
	6250 800  6350 800 
Wire Wire Line
	6350 900  5950 900 
Wire Wire Line
	6250 1000 6350 1000
Wire Wire Line
	6350 1100 5950 1100
Wire Wire Line
	6250 1200 6350 1200
Text GLabel 4350 900  0    60   Input ~ 0
OE
Text GLabel 4500 900  2    60   Input ~ 0
GND
Wire Wire Line
	4500 900  4350 900 
$EndSCHEMATC
