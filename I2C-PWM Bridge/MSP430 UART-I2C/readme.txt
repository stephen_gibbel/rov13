Goal:
    MSP430-based UART-I2C bridge, supporting general read/write
    operations to I2C slave devices. AKA an exercise in converting
    between strings and numerical types as quickly as possible. :-)

Commands (Delimited by newlines for easy testing/human use):
* Write to device:
    W40 DE AD BE EF 00 00 FF 00 -
   
    Writes the 8 bytes specified to device at i2c address 0x40 (hex)
    
    Responds with return value of 0 through 4 as defined by Wire.endTransmission
    
    Optional "-" at end causes device to send a RESTART at the end
    of transmission, instead of a STOP. Useful for requesting specific
    data from a device. **Note this doesn't release the i2c bus.**
   
* Read from device:
    R40 12 -
    
    Reads 12 bytes from device at i2c address 0x40 (hex)
    Note 12 is a decimal value, not hex.
    
    Responds with space delimited hex values returned from device.
    
    Optional "-" at end causes the same thing as described above in the
    "Write to device" note.

Errata:
    Wire library port for Energia (MSP430) is slightly broken, RESTART
    feature does not work, so don't use the hyphen at the end of a
    command. (This should work if we use an actual atmega)
    
To do:
    Optimize code. -DONE

    i2c buffer can be removed and Wire library's buffer
    used instead, saves a couple loops and some bytes of memory. -DONE
    
    Optimize the hex-as-two-ASCII-values --> uint8_t conversion
    function.

    Add i2c timeout support to Wire.cpp in Energia/Arduino core.
    
    Either fix RESTART support somehow or just compile on Arduino
    and run on ATMEGA.