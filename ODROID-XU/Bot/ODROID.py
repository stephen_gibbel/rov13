#!/usr/bin/env python
import Pyro4
import socket
from pwmController2 import *


class MotorController():
	def __init__(self):
		self.defaultFrequency = 100
		self.defaultCPUClockMHz = 26.75
		self.pwmBoard = pwmController2('/dev/ttyUSB0', pwmBoardAddress = 0x40, pwmBoardClockrate = self.defaultCPUClockMHz*10**6)
		self.pwmBoard.initializeDevice()
		self.pwmBoard.setFrequency(int(self.defaultFrequency))
		self.motorVars = []

	def updateMotors(self, mV):
		self.motorVars = mV
		try:
			self.pwmBoard.setPWMByDutyLikeServo(0, [self.motorVars[i] for i in range(len(self.motorVars))])
		except Exception:
			print 'Something went wrong'

motor_controller = MotorController()

Pyro4.config.HOST = "192.168.1.1"
daemon = Pyro4.Daemon()
ns = Pyro4.locateNS()
uri = daemon.register(motor_controller)
ns.register("motorcontroller", uri)

daemon.requestLoop() 
