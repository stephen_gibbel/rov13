#!/usr/bin/env python
from i2cController_bridge import *
import time

# Devon Goode 2013. pwmController class to control Adafruit PCA9685-based
# 16-channel PWM board. This library is designed to be used in conjunction with
# the i2c-uart bridge to control the PWM board. Has most basic features,
# including setting the PWM based on duty cycle desired or based on RC throttle/
# servo 1ms-to-2ms control range. Automatically converts from desired servo
# "percentage" to a value between 1ms and 2ms regardless of board output frequency.
#
# Future, probably not going to happen features include ability to use external
# time sources for the board using EXTCLK setting, options to tweak all MODE1 and
# MODE2 settings and treat them as a dictionary of booleans...
#
# This is pwmController2.py. Because we needed a new one and this was the 
# responsible way to name it. :-P
#
class pwmController2(i2cController):
    def __init__(self, serialPort, baud = 115200, pwmBoardAddress = 0x40, pwmBoardClockrate = 25000000):
        super(pwmController2, self).__init__(serialPort, baud)
        self.pwmBoardAddress = pwmBoardAddress
        self.pwmBoardClockrate = pwmBoardClockrate
        self.pwmOutputClockrate = 0
        self.MODE1 = 0x00
        self.MODE2 = 0x00
        self.initialized = False # Since it was just created, it hasn't been initialized yet.

    # This one does what it says on the tin.
    def initializeDevice(self):
        if not self.initialized:
            self.softwareReset() # Get device into known state by resetting it.
            # Grab values from MODE1 and MODE2 registers.
            # Then grab prescale value to find output
            # PWM frequency from that.
            self.MODE1 = self.readRegisters(self.pwmBoardAddress, 0x00, 1)[0]
            self.MODE2 = self.readRegisters(self.pwmBoardAddress, 0x01, 1)[0]
            prescaleValue = self.readRegisters(self.pwmBoardAddress, 0xFE, 1)[0]
            self.pwmOutputClockrate = self.pwmBoardClockrate / (4096. * (prescaleValue + 1))
            # Set AI (bit 5 of the mode 1 register).
            self.MODE1 = self.MODE1 | 0b00100000
            # Save MODE1's new settings back to the board.
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
            # Now wake board up.
            self.wakeUp()
            self.initialized = True
    
    # Do not use this. Unless you really want to. Then you gotta recalculate
    # the PWM clockrate.
    def setBoardCPUClock(self, CPUfreqMHz):
        self.pwmBoardClockrate = int(float(CPUfreqMHz)*10**6)
        self.pwmOutputClockrate = self.pwmBoardClockrate / (4096. * ((int(self.pwmBoardClockrate / (4096 * self.pwmOutputClockrate)) - 1) + 1))
        return self.pwmOutputClockrate
        
    # Get the prescale value from the board, calculate the actual clock (update) rate
    # and return it.
    def getFrequency(self):
        prescaleValue = self.readRegisters(self.pwmBoardAddress, 0xFE, 1)[0]
        self.pwmOutputClockrate = self.pwmBoardClockrate / (4096. * (prescaleValue + 1))
        return self.pwmOutputClockrate
    
    # Take a frequency, turn it into a prescale value. Then write this to the board.
    # Then use that prescale value and recalculate the actual frequency we just
    # set the board to, and return that value.
    def setFrequency(self, freq):
        self.goToSleep()
        prescaleValue = int(self.pwmBoardClockrate / (4096 * freq)) - 1
        if prescaleValue < 3:
            raise ValueError("Prescale value ended up less than 3. Try a lower PWM frequency.")
        elif prescaleValue > 255:
            raise ValueError("Prescale value over 255. Try a higher PWM frequency.")
        self.writeRegisters(self.pwmBoardAddress, 0xFE, [prescaleValue])
        self.wakeUp()
        # Recalculate output clockrate based on derived prescale value.
        # Rounding has probably changed the actual clockrate slightly.
        # So we will return it.
        self.pwmOutputClockrate = self.pwmBoardClockrate / (4096. * (prescaleValue + 1))
        return self.pwmOutputClockrate

    # Make board go to sleep. Used for setFrequency. This pauses operation
    # but saves PWM settings.
    def goToSleep(self):
        self.MODE1 = self.MODE1 | 0b00010000
        self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
    
    # Wake up the board. Resume where we left off!
    def wakeUp(self):
        self.MODE1 = self.readRegisters(self.pwmBoardAddress, 0x00, 1)[0]
        # Check if bit 7 of the MODE1 register (reset bit) is set.
        # If so, clear bit 4 (sleep), wait 1ms, "set" bit 7 (which magically clears reset)
        # Otherwise just clear sleep (bit 4).
        if self.MODE1 & (1 << 7) != 0:
            self.MODE1 = self.MODE1 & 0b11101111
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1 & 0b01111111])
            time.sleep(0.001)
            self.MODE1 = self.MODE1 | 0b10000000
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
        else:
            # Clear bit 4, aka take board out of sleep mode.
            self.MODE1 = self.MODE1 & 0b11101111
            self.writeRegisters(self.pwmBoardAddress, 0x00, [self.MODE1])
        
    # Perform software reset as explained in section 7.6 of datasheet.
    # Icky i2c features used directly in here. Oh well. i2cController
    # is supposed to be general purpose and this is proprietary to
    # the PCA chip.
    def softwareReset(self):
        self.i2cReset()
# -------------------------------------- PWM Stuff Below --------------------------------------------
# Duty cycle percentage methods below.
    # Apologies in advance for Java-esque method naming...
    # This monster returns a list formatted as required for
    # writing to the registers of the PCA chip.
    #   ON_LOW_BIT ON_HIGH_BIT OFF_LOW_BIT OFF_HIGH_BIT
    def getTimerValuesForDutyCyclePWM(self, dutyCycle, offset = 0):
        if dutyCycle >= 0 and dutyCycle <= 100:
            onValue = offset
            onDuration = int(dutyCycle * 40.95)
            if onDuration == 0:
                onValue  = 0b0000000000000000
                offValue = 0b0001000000000000
            elif onDuration == 4095:
                onValue  = 0b0001000000000000
                offValue = 0b0000000000000000
            else:
                if onValue + onDuration < 4095:
                    offValue = onValue + onDuration
                else:
                    offValue = (onValue + onDuration) - 4095
        else:
            raise ValueError("Duty cycle out of range.")
        offValue = divmod(offValue, 0x100)
        onValue = divmod(onValue, 0x100)
        return [onValue[1], onValue[0], offValue[1], offValue[0]]

    # Set a number of PWM channels.
    def setPWMByDutyCycle(self, startChannel, dutyCycles):
        if len(dutyCycles) > 8:
            raise ValueError("Too many channels. i2c bridge can only handle 8 at once...")
        elif (startChannel + len(dutyCycles)) > 16:
            raise ValueError("Went past maximum possible channel.")
        onAndOffValues = map(self.getTimerValuesForDutyCyclePWM, dutyCycles)
        startRegister = 4 * startChannel + 6
        onAndOffValues = [item for sublist in onAndOffValues for item in sublist]
        self.writeRegisters(self.pwmBoardAddress, startRegister, onAndOffValues)
        
#--------------------------------------------------------------------------------------------------------
# Servo-style PWM methods.
    def getTimerValuesForServoPWM(self, throttlePercentage):
        if throttlePercentage >= 0 and throttlePercentage <= 100:
            # Find how many timer-ticks to get 1ms.
            # Do 1ms-worth-of-ticks + 1ms worth-of-ticks * throttlePercentage/100.
            # Ticks per millisecond:
            #   - There are pwmOutputClockrate PWM iterations per second
            #   - There are 4096 ticks per iteration.
            #   - pwmOutputClockrate iterations per second * 4096 ticks per iteration = # of ticks per second
            #
            #   - ticks per second / 1000 = ticks per millisecond
            #
            ticksIn1ms = self.pwmOutputClockrate * 4.096
            if ticksIn1ms < 100:
                print "[!] Warning: Tick resolution too small for accurate servo-style PWM. Try higher PWM frequency."
            onValue = 0
            offValue = int(ticksIn1ms + throttlePercentage / 100. * ticksIn1ms)
        else:
            raise ValueError("Throttle percentage out of range.")
        # Now we found offValue, make sure it's reasonable, return registers.
        if offValue <= 4095:
            onValue = divmod(onValue, 0x100)
            offValue = divmod(offValue, 0x100)
        else:
            raise ValueError("Throttle percentage impossible with current PWM frequency setting.")
        return [onValue[1], onValue[0], offValue[1], offValue[0]]

    def setPWMByDutyLikeServo(self, startChannel, throttlePercentages):
        if len(throttlePercentages) > 8:
            raise ValueError("Too many channels given. i2c bridge can only handle 8 at once...")
        elif (startChannel + len(throttlePercentages)) > 16:
            raise ValueError("Went past maximum possible channel.")
        onAndOffValues = map(self.getTimerValuesForServoPWM, throttlePercentages)
        startRegister = 4 * startChannel + 6
        onAndOffValues = [item for sublist in onAndOffValues for item in sublist]
        self.writeRegisters(self.pwmBoardAddress, startRegister, onAndOffValues)
#--------------------------------------------------------------------------------------------------------
# Raw millisecond pulse sender, since apparently some servos like to use more than
# the normal 1ms - 2ms range and I really want to play with them.
    def getTimerValuesForMillisecondPWM(self, numberOfMilliseconds):
        ticksIn1ms = self.pwmOutputClockrate * 4.096
        onValue = 0
        offValue = int(ticksIn1ms * numberOfMilliseconds)
    # Now we found offValue, make sure it's reasonable, return registers.
        if offValue <= 4095:
            onValue = divmod(onValue, 0x100)
            offValue = divmod(offValue, 0x100)
        else:
            raise ValueError("Millisecond request impossible with current PWM frequency setting.")
        return [onValue[1], onValue[0], offValue[1], offValue[0]]

    def setPWMByDutyByMillisecondsPWM(self, startChannel, numberOfMilliseconds):
        if len(numberOfMilliseconds) > 8:
            raise ValueError("Too many channels given. i2c bridge can only handle 8 at once...")
        elif (startChannel + len(numberOfMilliseconds)) > 16:
            raise ValueError("Went past maximum possible channel.")
        onAndOffValues = map(self.getTimerValuesForMillisecondPWM, numberOfMilliseconds)
        startRegister = 4 * startChannel + 6
        onAndOffValues = [item for sublist in onAndOffValues for item in sublist]
        self.writeRegisters(self.pwmBoardAddress, startRegister, onAndOffValues)