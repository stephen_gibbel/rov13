#/bin/sh

while true; do
        # Start Pyro nameserver:
        echo "Starting nameserver."
        python -m Pyro4.naming -n 192.168.1.1 &
        echo "Nameserver started. Launching listener."
        nsPID=$!
        python ~/rov13/ODROID-XU/Bot/ODROID.py

        echo "Python exited, shutting off nameserver."
        kill $nsPID
        echo "Nameserver terminated. Done!"
done
