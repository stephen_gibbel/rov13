#!/usr/bin/env python
import socket  #Used for communicating with an Arduino

def checkMax(value, range):
    if (value > range[1]):
        value = range[1]
    elif (value < range[0]):
        value = range[0]
    return value

def scale(value, sourceRange, destinationRange):
    """Scales a value from one range to another."""
    return (((value - sourceRange[0]) / (sourceRange[1]-sourceRange[0])) * 
                (destinationRange[1] -destinationRange[0]) +destinationRange[0])

def proportionalControl(originalValue, minValue, maxValue, scale):
    """Used to increase the joystick's lower range for more precise control."""
    if (originalValue >= 0):
        proportionalValue = originalValue**scale
    else:
        proportionalValue = - abs(originalValue**scale)
    return proportionalValue

def sendMotorOutput(joystickValues, motorController):
    """Takes a list of joystick axis values and sends them to the remote motor
       controller class.

       The joystickValues list should be structured as follows:
       left x-axis, left y-axis, right x-axis, right y-axis, z-axis
    """

    outputMotorVars = [0, 0, 0, 0, 0, 0, 0, 0] #Holds temporary output values.
    #outputMotorVars = [top front left, top front right,
    #                       top rear left, top rear right,
    #                       forward left, forward right,
    #                       strafe front, strafe rear]

    pwmReverse = [1, 1, -1, 1, 1, 1, -1, 1]  #1 is forward, -1 is reversed
    pwmMapping = [0, 1, 2, 3,
                  4, 5, 6, 7]  #remap channels if needed
    # This mapping gives us: 
    #   - Top Front Left, Top Rear Left, Forward Left, Strafe Front
    #   - Top Front Right, Top Rear Right, Forward Right, Strafe Rear

    #Set top motor values
    outputMotorVars[0] = joystickValues[3] + joystickValues[4]
    outputMotorVars[6] = joystickValues[3] + joystickValues[4]
    outputMotorVars[1] = - joystickValues[3] + joystickValues[4]
    outputMotorVars[4] = - joystickValues[3] + joystickValues[4]

    #Forward / turning thrusters
    outputMotorVars[2] = joystickValues[1] + joystickValues[2]
    outputMotorVars[5] = joystickValues[1] - joystickValues[2]

    #Strafe thrusters
    outputMotorVars[3] = joystickValues[0]
    outputMotorVars[7] = joystickValues[0]


    maxOutput = 4.      #Maximum number of motors that can be on at once, used
    #                     for limiting power consumption as to not trip a fuse.
    currentOutput = 0.  #Current as in iteration, not amps

    for x in range(len(outputMotorVars)):  #Sum up motor thrust values
        currentOutput += abs(outputMotorVars[x])
        if (outputMotorVars[x] > 1.):
            outputMotorVars[x] = 1.
        if (outputMotorVars[x] < -1.):
            outputMotorVars[x] = -1.

    if (currentOutput > maxOutput):  #If drawing too much power, limit to sF
        scalingFactor = maxOutput / currentOutput
        for x in range(len(outputMotorVars)):
            outputMotorVars[x] = outputMotorVars[x] * scalingFactor

    #Switch channels & reverse thrust if needed. Somewhat unsafe, be careful!
    outputMotorVarsTemp = [0, 0, 0, 0, 0, 0, 0, 0]
    for i in range(len(outputMotorVars)):
        outputMotorVarsTemp[i] = outputMotorVars[pwmMapping[i]] * pwmReverse[i]
    outputMotorVars = outputMotorVarsTemp

    #Tweak individual PWM channel timings
    outputMotorVars[0] = checkMax(outputMotorVars[0] + 0.1, (-1., 1.))
    outputMotorVars[1] = checkMax(outputMotorVars[1] + 0.11, (-1., 1.))
    outputMotorVars[2] = checkMax(outputMotorVars[2] + 0.1, (-1., 1.))
    outputMotorVars[3] = checkMax(outputMotorVars[3] + 0.1, (-1., 1.))
    outputMotorVars[4] = checkMax(outputMotorVars[4] + 0.05, (-1., 1.))
    outputMotorVars[5] = checkMax(outputMotorVars[5] + 0.07, (-1., 1.))
    outputMotorVars[6] = checkMax(outputMotorVars[6] + 0.13, (-1., 1.))
    outputMotorVars[7] = checkMax(outputMotorVars[7] + 0.1, (-1., 1.))


    #Remote control class uses a percentage scale while joysticks use -1 to 1
    transformedMotorVars = [50, 50, 50, 50, 50, 50, 50, 50]
    for i in range(len(transformedMotorVars)):
        transformedMotorVars[i] = scale(outputMotorVars[i], (-1.0, 1.0),(0., +100.))

    try:
        #print transformedMotorVars
        
        motorController.updateMotors(transformedMotorVars)
        #                             1 2  3 4 5 6 7 
        return True
    # Dirty hack to deal with fact that IOError produces a string 
    # the way IOError is raised on udp_motor_controller code.
    #except IOError as (errno, strerror):
    #    print "I/O error({0}): {1}".format(errno, strerror)
    #    return False
    except IOError as err:
        print "I/O error:",err
        return False


class udp_motor_controller:
    """Sends motor control signals over UDP to Arduino"""
    
    def __init__(self, ip = "192.168.1.10", port = 8888, timeout = 0.5):
        self.ip = ip
        self.port = port
        self.controlSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # Open a UDP socket
        self.controlSocket.settimeout(timeout) # Set blocking socket operation timeout...

    # Take list of motor values (0 to 100%) and scale them to 0-255, convert them to
    # ASCII characters and append them to the string of values to send to the Arduino.
    # Note you can also use any value range you want by passing valueRange = XX to this
    # method.
    #
    # Protocol for Arduino controller requires an "M" at the beginning of any motor
    # control packets, so this is prepended.
    #
    # Looks for confirmation packet from Arduino afterwards to verify operation.
    #
    # Warning: If you send less than 16 values, the remaining channels will be set
    # to 127/center.
    def updateMotors(self, listOfVars, valueRange = 100):
        sendPacket = "M"
        for x in listOfVars:
            scaleVal = int(round(x * 255 / float(valueRange)))
            sendPacket += chr(scaleVal)
        for x in range(17 - len(sendPacket)):
            sendPacket += chr(127)
        try:
            self.controlSocket.sendto(sendPacket, (self.ip, self.port))
        except:
            raise IOError("Failed to send packet")
        else:
            try:
                returnData, fromIP = self.controlSocket.recvfrom(64)
            except:
                raise IOError("Did not receive confirmation")
            else:
                if returnData != "OK":
                    raise IOError("Arduino failed to update PWM values" + "with error code " + returnData)
