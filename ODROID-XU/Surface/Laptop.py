#!/usr/bin/env python
#I'm off to make some dinner, here's what I've got so far. Mostly just reversed the channels so they're usable
#Sorry for the horrible uncommented mess, I'll get it cleaned up sooner or later
import Pyro4
import pygame
import time

def scale(val, src, dst):
    return ((val - src[0]) / (src[1]-src[0])) * (dst[1]-dst[0]) + dst[0]

def proportionalControl(originalValue, minValue, maxValue, scale):
    if (originalValue >= 0):
        proportionalValue = originalValue**scale
    else:
        proportionalValue = - originalValue**scale
    return proportionalValue

def sendMotorOutput(joystickValues, motorController):
    #joystickValues = [left x-axis, left y-axis, right x-axis, right y-axis, z-axis]

    #outputMotorVars = [top front left, top front right,
    #                    top rear left, top rear right,
    #                    forward left, forward right,
    #                    strafe front, strafe rear]

    outputMotorVars = [0, 0, 0, 0, 0, 0, 0, 0]

    outputMotorVars[0] = joystickValues[3] + joystickValues[4]
    outputMotorVars[1] = joystickValues[3] + joystickValues[4]
    outputMotorVars[2] = - joystickValues[3] + joystickValues[4]
    outputMotorVars[3] = - joystickValues[3] + joystickValues[4]

    outputMotorVars[4] = joystickValues[1] + joystickValues[2]
    outputMotorVars[5] = joystickValues[1] - joystickValues[2]

    outputMotorVars[6] = joystickValues[0]
    outputMotorVars[7] = joystickValues[0]

    maxOutput = 2.
    currentOutput = 0.
    for x in range(len(outputMotorVars)):
        currentOutput += abs(outputMotorVars[x])
        if (outputMotorVars[x] > 1.):
            outputMotorVars[x] = 1.
        if (outputMotorVars[x] < -1.):
            outputMotorVars[x] = -1.

    if (currentOutput > maxOutput):
        scalingFactor = maxOutput / currentOutput
        for x in range(len(outputMotorVars)):
            outputMotorVars[x] = outputMotorVars[x] * scalingFactor

    transformedMotorVars = [50, 50, 50, 50, 50, 50, 50, 50]
    transformedMotorVars[0] = scale(outputMotorVars[0], (-1.0, 1.0), (0.,+100.))
    transformedMotorVars[1] = scale(outputMotorVars[1], (-1.0, 1.0), (0.,+100.))
    transformedMotorVars[2] = scale(outputMotorVars[2], (-1.0, 1.0), (0.,+100.))
    transformedMotorVars[3] = scale(outputMotorVars[3], (-1.0, 1.0), (0.,+100.))
    transformedMotorVars[4] = scale(outputMotorVars[4], (-1.0, 1.0), (0.,+100.))
    transformedMotorVars[5] = scale(outputMotorVars[5], (-1.0, 1.0), (0.,+100.))
    transformedMotorVars[6] = scale(outputMotorVars[6], (-1.0, 1.0), (0.,+100.))
    transformedMotorVars[7] = scale(outputMotorVars[7], (-1.0, 1.0), (0.,+100.))

    motorController.updateMotors(transformedMotorVars)
    print transformedMotorVars

nameserver = Pyro4.locateNS()
uri = nameserver.lookup("motorcontroller")
motor_controller = Pyro4.Proxy(uri)

pygame.init()
clock = pygame.time.Clock()

if pygame.joystick.get_count() == 0:
    raise IOError("No joystick detected")
joystick = pygame.joystick.Joystick(0)
joystick.init()

stickLeftXAxis = 0
stickLeftYAxis = 1

stickRightXAxis = 3
stickRightYAxis = 4

triggerLeftZAxis = 2
triggerLeftValue = -1.
triggerRightZAxis = 5
triggerRightValue = -1.
isLogitechJoystick = True #These things have a floating zero on the triggers that only resets when you first bump them
logitechJoystickBumpedLeftTriggered = False
logitechJoystickBumpedRightTriggered = False

proportionalControlConstant = 2

exit = False
count = 0
while (exit == False):
    pygame.event.pump()

    if (isLogitechJoystick == True):
        if (logitechJoystickBumpedLeftTriggered == False and joystick.get_axis(triggerLeftZAxis) != 0.0):
            logitechJoystickBumpedLeftTriggered = True
            triggerLeftValue = joystick.get_axis(triggerLeftZAxis)
        elif (logitechJoystickBumpedLeftTriggered == True):
            triggerLeftValue = joystick.get_axis(triggerLeftZAxis)

        if (logitechJoystickBumpedRightTriggered == False and joystick.get_axis(triggerRightZAxis) != 0.0):
            logitechJoystickBumpedRightTriggered = True
            triggerRightValue = joystick.get_axis(triggerRightZAxis)
        elif (logitechJoystickBumpedRightTriggered == True):
            triggerRightValue = joystick.get_axis(triggerRightZAxis)
    else:
        triggerLeftValue = joystick.get_axis(triggerLeftZAxis)
        triggerRightValue = joystick.get_axis(triggerRightZAxis)

    sendMotorOutput([proportionalControl(joystick.get_axis(stickLeftXAxis), -1, 1, proportionalControlConstant),
                    proportionalControl(-joystick.get_axis(stickLeftYAxis), -1, 1, proportionalControlConstant),
                    proportionalControl(joystick.get_axis(stickRightXAxis), -1, 1, proportionalControlConstant),
                    proportionalControl(-joystick.get_axis(stickRightYAxis), -1, 1, proportionalControlConstant),
                    proportionalControl(((1.+triggerRightValue)/2.) -((1.+triggerLeftValue)/2.), -1, 1, proportionalControlConstant)],
                    motor_controller)
    clock.tick(120)
    #time.sleep(.0001)
