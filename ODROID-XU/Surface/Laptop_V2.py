#!/usr/bin/env python
import Pyro4   #Networking library used for RPC
import pygame  #Provides joystick and timing classes
import time    #Used for secondary timing control, probably not needed

from ExternalFunctions import *  #Pull the magic out of the hat


outputDevice = 'arduino' #Output device is either 'arduino' or 'odroid'

if (outputDevice.lower() == 'odroid'):
    try:
        nameserver = Pyro4.locateNS()
        uri = nameserver.lookup("motorcontroller")
        motor_controller = Pyro4.Proxy(uri)
    except Pyro4.errors.NamingError:
        print 'Nameserver not detected.'
    except Pyro4.errors.PyroError:
        print 'Generic Pyro4 error.'

elif (outputDevice.lower() == 'arduino'):
    motor_controller = udp_motor_controller()

try:
    pygame.init()
    clock = pygame.time.Clock()

    if pygame.joystick.get_count() == 0:
        raise IOError("Joystick not detected.")
    joystick = pygame.joystick.Joystick(0)
    joystick.init()
except IOError:
    print 'Error initializing pygame.'

stickLeftXAxis = 0
stickLeftYAxis = 1

stickRightXAxis = 3
stickRightYAxis = 4

triggerLeftZAxis = 2
triggerLeftValue = -1.
triggerRightZAxis = 5
triggerRightValue = -1.
isLogitechJoystick = True  #Fix pygame uninitialized joystick value bug if so
logitechJoystickBumpedLeftTriggered = False
logitechJoystickBumpedRightTriggered = False

proportionalControlConstant = 2  #Increase sensitivity in center of joystick


exit = False
while (exit == False):
    pygame.event.pump()

    if (isLogitechJoystick == True):
        if (logitechJoystickBumpedLeftTriggered == False and 
            joystick.get_axis(triggerLeftZAxis) != 0.0):

            logitechJoystickBumpedLeftTriggered = True
            triggerLeftValue = joystick.get_axis(triggerLeftZAxis)
        elif (logitechJoystickBumpedLeftTriggered == True):
            triggerLeftValue = joystick.get_axis(triggerLeftZAxis)

        if (logitechJoystickBumpedRightTriggered == False and 
            joystick.get_axis(triggerRightZAxis) != 0.0):
        
            logitechJoystickBumpedRightTriggered = True
            triggerRightValue = joystick.get_axis(triggerRightZAxis)
        elif (logitechJoystickBumpedRightTriggered == True):
            triggerRightValue = joystick.get_axis(triggerRightZAxis)
    else:
        triggerLeftValue = joystick.get_axis(triggerLeftZAxis)
        triggerRightValue = joystick.get_axis(triggerRightZAxis)

    if (sendMotorOutput([proportionalControl(joystick.get_axis(stickLeftXAxis), 
                         -1, 1, proportionalControlConstant),
                         proportionalControl(-joystick.get_axis(stickLeftYAxis),
                         -1, 1, proportionalControlConstant),
                         proportionalControl(joystick.get_axis(stickRightXAxis), 
                         -1, 1, proportionalControlConstant),
                         proportionalControl(-joystick.get_axis(stickRightYAxis), 
                         -1, 1, proportionalControlConstant),
                         proportionalControl(((1.+triggerRightValue)/2.) 
                         - ((1.+triggerLeftValue)/2.), -1, 1, 
                         proportionalControlConstant)],
        motor_controller) == False):

        print 'Error sending control signal'

    clock.tick(120)
    #time.sleep(.0001)
