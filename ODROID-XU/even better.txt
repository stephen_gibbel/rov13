!!! Important note: Disregard all this. PS3 Eye doesn't appear to support mjpeg after all. Whoops... :(
		Well that's a rotten shame... Could've sword it did

ffmpeg nonsense begone! We've got MJPG-streamer!

http://sourceforge.net/projects/mjpg-streamer/

This little puppy grabs the MJPEG frames straight from UVC interface of the webcam, and streams them via its built-in web server! Lightning fast, uses 3MB RAM, nearly zero latency (I'd estimate a few milliseconds tops) even with my crappy Dynex 3 megapixel webcam! CPU usage on dual core pentium g620 is under 1%. SO LEGIT! :D

This is basically an implementation of the ancient MJPEG streaming via HTTP standard:
http://en.wikipedia.org/wiki/Motion_JPEG#M-JPEG_over_HTTP

Basically just a GET request that has a MIME type indicating it's a stream and continuously transmits data while a server connection is open. Just like back in the good old Netscape 3 days! :)

The built-in web server streams excellently to Firefox and Chrome (and the HTML required is literally <img src="/?action=stream" />), the Java applet it comes with (cambozola, which is open source) streams even smoother. Implementing support for this would be braindeadedly simple. Heck, there's a Javascript example where it just grabs snapshots rapidly and even *that's* giving me zero latency.

Steps to amazement:
1. svn co https://svn.code.sf.net/p/mjpg-streamer/code/mjpg-streamer/ mjpg-streamer
2. *fight with dependencies*
3. make
4. sudo checkinstall -D make install       //makes a debian package
5. export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib    //Because Debian doesn't use /usr/local so libs aren't in path.
6. mjpg_streamer -i "input_uvc.so -d /dev/video0 -r 640x480 -f 30" -o "output_http.so -w /usr/local/www"
7. Visit http://localhost:8080/

Alternatively modify it to use the right paths so exporting ld libs isn't necessary.