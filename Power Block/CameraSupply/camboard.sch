EESchema Schematic File Version 2  date Wed 14 May 2014 03:56:36 PM PDT
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:coseldcdc
EELAYER 24 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "14 may 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L QW050B1 PS1
U 1 1 5373F389
P 2700 2350
F 0 "PS1" H 2200 2950 60  0000 C CNN
F 1 "QW050B1" H 2700 1600 60  0000 C CNN
F 2 "" H 2700 2350 60  0000 C CNN
F 3 "" H 2700 2350 60  0000 C CNN
	1    2700 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2350 1750 2350
Wire Wire Line
	1750 2350 1750 2650
Wire Wire Line
	1000 2650 1850 2650
NoConn ~ 1850 2900
NoConn ~ 3550 2350
Wire Wire Line
	3550 2150 3550 1950
Wire Wire Line
	3550 2550 3550 2750
$Comp
L DIODE D1
U 1 1 5373F3A3
P 1200 2050
F 0 "D1" H 1200 2150 40  0000 C CNN
F 1 "DIODE" H 1200 1950 40  0000 C CNN
F 2 "~" H 1200 2050 60  0000 C CNN
F 3 "~" H 1200 2050 60  0000 C CNN
	1    1200 2050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C1
U 1 1 5373F3C1
P 1550 2350
F 0 "C1" H 1600 2450 50  0000 L CNN
F 1 "33uF" H 1600 2250 50  0000 L CNN
F 2 "~" H 1550 2350 60  0000 C CNN
F 3 "~" H 1550 2350 60  0000 C CNN
	1    1550 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 2550 1550 2650
Connection ~ 1750 2650
Wire Wire Line
	1400 2050 1850 2050
Wire Wire Line
	1550 2150 1550 2050
Connection ~ 1550 2050
$Comp
L CP1 C2
U 1 1 5373F3FE
P 4050 2350
F 0 "C2" H 4100 2450 50  0000 L CNN
F 1 "330uF" H 4100 2250 50  0000 L CNN
F 2 "~" H 4050 2350 60  0000 C CNN
F 3 "~" H 4050 2350 60  0000 C CNN
	1    4050 2350
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 5373F40D
P 4450 2350
F 0 "C3" H 4450 2450 40  0000 L CNN
F 1 "1.0uF" H 4456 2265 40  0000 L CNN
F 2 "~" H 4488 2200 30  0000 C CNN
F 3 "~" H 4450 2350 60  0000 C CNN
	1    4450 2350
	1    0    0    -1  
$EndComp
$Comp
L LED D2
U 1 1 5373F430
P 4850 2450
F 0 "D2" H 4850 2550 50  0000 C CNN
F 1 "Cyan" H 4850 2350 50  0000 C CNN
F 2 "~" H 4850 2450 60  0000 C CNN
F 3 "~" H 4850 2450 60  0000 C CNN
	1    4850 2450
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 5373F43F
P 5100 2300
F 0 "R1" V 5180 2300 40  0000 C CNN
F 1 "470" V 5107 2301 40  0000 C CNN
F 2 "~" V 5030 2300 30  0000 C CNN
F 3 "~" H 5100 2300 30  0000 C CNN
	1    5100 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1950 5550 1950
Wire Wire Line
	5100 1950 5100 2050
Wire Wire Line
	4450 1950 4450 2150
Connection ~ 4450 1950
Wire Wire Line
	4050 1950 4050 2150
Connection ~ 4050 1950
Wire Wire Line
	3550 2750 5550 2750
Wire Wire Line
	4850 2750 4850 2650
Wire Wire Line
	4450 2550 4450 2750
Connection ~ 4450 2750
Wire Wire Line
	4050 2550 4050 2750
Connection ~ 4050 2750
Wire Wire Line
	4850 2250 5000 2250
Wire Wire Line
	5000 2250 5000 2550
Wire Wire Line
	5000 2550 5100 2550
$Comp
L CONN_2 P2
U 1 1 5373F4E0
P 5900 2350
F 0 "P2" V 5850 2350 40  0000 C CNN
F 1 "CONN_2" V 5950 2350 40  0000 C CNN
F 2 "~" H 5900 2350 60  0000 C CNN
F 3 "~" H 5900 2350 60  0000 C CNN
	1    5900 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1950 5550 2250
Connection ~ 5100 1950
Wire Wire Line
	5550 2750 5550 2450
Connection ~ 4850 2750
$Comp
L CONN_2 P1
U 1 1 5373F525
P 650 2300
F 0 "P1" V 600 2300 40  0000 C CNN
F 1 "CONN_2" V 700 2300 40  0000 C CNN
F 2 "~" H 650 2300 60  0000 C CNN
F 3 "~" H 650 2300 60  0000 C CNN
	1    650  2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	1000 2050 1000 2200
Wire Wire Line
	1000 2400 1000 2650
Connection ~ 1550 2650
$EndSCHEMATC
