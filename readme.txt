"Where are we going?" I asked
"I don't know," he said. "Just driving."
"But this road doesn't go anywhere," I told him.
"That doesn't matter."
"What does?" I asked, after a little while.
"Just that we're on it, dude," he said.
	--	Bret Easton Ellis