// ESC wants a servo-compatible PWM signal so
// we'll use the handy-dandy servo library.
#include <Servo.h>
Servo c1;
Servo c2;
Servo c3;

// Configure pins for potentiometer and
// servo-compatible PWM output.
#define c1Input A0
#define c1Output 0

#define c2Input A1
#define c2Output 1

#define c3Input A2
#define c3Output 2

#define minThrottle 1000
#define maxThrottle 2000

// Couple of variables we'll use to temporarily
// store things later.
int c1InputValue;
int c1OutputScaledValue;

int c2InputValue;
int c2OutputScaledValue;

int c3InputValue;
int c3OutputScaledValue;

int throttleRange = maxThrottle - minThrottle;
int halfThrottle = minThrottle + (throttleRange / 2);
int deadZone = throttleRange / 25;

float c1OutFloat;
float c2OutFloat;
float c3OutFloat;

float left;
float right;
float k1 = 1;
float k2 = 1;



// Now we attach the Servo object to the output pin
// and tell it to use PWM pulses in the range of
// 1 to 2 milliseconds.
void setup()
{
  c1.attach(c1Output, minThrottle, maxThrottle);
  c2.attach(c2Output, minThrottle, maxThrottle);
  c3.attach(c3Output, minThrottle, maxThrottle);
}

// In here we read a value ranging from 0 to 1023 from our analog
// input. Scale it to the appropriate range. Then we apply boundary
// conditions if necessary.
// Then we send the servo signal to the PWM pin.
void loop()
{  
  
  c1InputValue = map(analogRead(c1Input), 0, 1023, -100, 100);
  c2InputValue = map(analogRead(c2Input), 0, 1023, -100, 100);
  c3InputValue = analogRead(c3Input);

  left = (int)(k1 * (float)(c1InputValue + c2InputValue) );
  right = (int)(k2 * (float)(c1InputValue - c2InputValue) );
  
  //  input value
  // -------------  x output range + output offset = new value.
  //  input range
  c1OutputScaledValue = (int)((float)map(left, -100, 100, 0, 1023) / 1022 * throttleRange) + minThrottle;
  c2OutputScaledValue = (int)((float)map(right, -100, 100, 0, 1023) / 1022 * throttleRange) + minThrottle;
  c3OutputScaledValue = (int)((float)c3InputValue / 1022 * throttleRange) + minThrottle;
  
  
  // Is our output value too high or too low? (Should never happen.) Or is
  // it somewhere near the center of the range, in which case we should apply
  // the deadzone to it.
  if (c1OutputScaledValue >= maxThrottle) {
    c1OutputScaledValue = maxThrottle;
  } else if (c1OutputScaledValue <= minThrottle) {
    c1OutputScaledValue = minThrottle;
  } else if (c1OutputScaledValue > (halfThrottle - deadZone) && c1OutputScaledValue < (halfThrottle + deadZone)) {
    c1OutputScaledValue = halfThrottle;
  }
  
  
  if (c2OutputScaledValue >= maxThrottle) {
    c2OutputScaledValue = maxThrottle;
  } else if (c2OutputScaledValue <= minThrottle) {
    c2OutputScaledValue = minThrottle;
  } else if (c2OutputScaledValue > (halfThrottle - deadZone) && c2OutputScaledValue < (halfThrottle + deadZone)) {
    c2OutputScaledValue = halfThrottle;
  }
  
  if (c3OutputScaledValue >= maxThrottle) {
    c3OutputScaledValue = maxThrottle;
  } else if (c2OutputScaledValue <= minThrottle) {
    c3OutputScaledValue = minThrottle;
  } else if (c3OutputScaledValue > (halfThrottle - deadZone) && c3OutputScaledValue < (halfThrottle + deadZone)) {
    c3OutputScaledValue = halfThrottle;
  }
  
  c1.writeMicroseconds(c1OutputScaledValue);
  c2.writeMicroseconds(c2OutputScaledValue);
  c3.writeMicroseconds(c3OutputScaledValue);
  delay(10);
}

